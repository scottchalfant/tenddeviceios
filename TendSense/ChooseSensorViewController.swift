//
//  ChooseSensorViewController.swift
//  TendSense
//
//  Created by Scott Chalfant on 11/4/15.
//  Copyright © 2015 Tend. All rights reserved.
//

import Foundation
import UIKit

class ChooseSensorViewController : UITableViewController, SensorManagerListener {
    
    override func viewWillAppear(animated: Bool) {
        SensorManager.instance.discoverSensors()
        if let s = SensorManager.instance.lastSensor {
            sensors.append(s)
            tableView.selectRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0), animated: false, scrollPosition: UITableViewScrollPosition.Top)
        }
        SensorManager.instance.listeners.append(self)
    }
    override func viewWillDisappear(animated: Bool) {
        SensorManager.instance.stopDiscoveringSensors()
        SensorManager.instance.listeners.removeLast()
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        if indexPath.row < sensors.count {
            let s = sensors[indexPath.row]
            SensorManager.instance.lastSensor = s
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    var sensors = [Sensor]()
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sensors.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let sensor = sensors[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")!
        if let _ = cell.contentView.viewWithTag(3) as? UIImageView {
            // ??
        }
        if let label = cell.contentView.viewWithTag(1) as? UILabel {
            label.text = sensor.name
        }
        if let label = cell.contentView.viewWithTag(2) as? UILabel {
            label.text = sensor.id
        }
        return cell
    }
    
    
    /////
    
    func sensorManagerError(message: String) {
        
        print("Error: \(message)")
    }
    
    func sensorManagerFoundSensor(sensor: Sensor) {
        
        for s in sensors {
            if s.peripheral === sensor.peripheral {
                return
            }
        }
        
        // add
        sensors.append(sensor);
        tableView.reloadData()
        
    }
    func sensorManagerReady() {
        
        
    }
}