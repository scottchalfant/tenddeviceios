//
//  AnalyzeDataViewController.swift
//  TendSense
//
//  Created by Scott Chalfant on 1/7/16.
//  Copyright © 2016 Tend. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

class AnalyzeDataViewController : UITableViewController , MFMailComposeViewControllerDelegate{
    override func viewDidLoad() {
        self.hidesBottomBarWhenPushed = true
    }
    
    let emptySection = "[Data Outside Regions]"
    
    var sections : [String]!

    var dateFormatter : NSDateFormatter!
    
    @IBAction func emailButtonTouched(sender: AnyObject) {
        
        if( MFMailComposeViewController.canSendMail() ) {

            
            let mailComposer = MFMailComposeViewController()
            mailComposer.mailComposeDelegate = self
            
            //Set the subject and message of the email
            mailComposer.setSubject("Tend Sensor Data")
            
            var desc : String
            let uid : UIDevice = UIDevice.currentDevice()
            desc = uid.modelName
            desc += "_" + uid.systemName
            desc += "_" + uid.systemVersion
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyyMMddHHmmss"
            desc += "_" + dateFormatter.stringFromDate(NSDate())

            
            // body
            mailComposer.setMessageBody("Tend sensor data is attached.\n\n\(desc)", isHTML: false)

            
            if let d = NSData(contentsOfFile: SensorData.instance.fileName) {
                mailComposer.addAttachmentData(d, mimeType: "text/csv", fileName: "data_\(desc).csv")
            }
            if let d = NSData(contentsOfFile: SensorData.instance.regionsFileName) {
                mailComposer.addAttachmentData(d, mimeType: "text/csv", fileName: "regions_\(desc).csv")
            }

            self.presentViewController(mailComposer, animated: true, completion: nil)
        } else {
            Util.alert(self, title: "Cannot Send Mail", message: "Cannot send mail, sorry!  Note that for this to work you have to have an account set up in the default Apple email app.", buttons: ["OK"])
        }
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    override func viewWillAppear(animated: Bool) {
        dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd\nHH:mm:ss"
        
        sections = [String]()
        
        for r in SensorData.instance.regions {
            sections.append(r.name ?? "[Unamed Region]")
        }
        
        if !SensorData.instance.itemsWithoutRegions().isEmpty {
            sections.append(emptySection)
        }
        
    }
    
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionName = sections[section]
        if sectionName == emptySection {
            return SensorData.instance.itemsWithoutRegions().count
        } else {
            let region = SensorData.instance.regions[section]
            return SensorData.instance.itemsForRegion(region).count
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section]
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")!

        let items : [SensorDataItem]
        // get items for section
        let sectionName = sections[indexPath.section]
        if sectionName == emptySection {
            items = SensorData.instance.itemsWithoutRegions()
        } else {
            let region = SensorData.instance.regions[indexPath.section]
            items = SensorData.instance.itemsForRegion(region)
        }
        let item = items[indexPath.row]
    
        // date
        if let label:UILabel = cell.contentView.viewWithTag(1) as? UILabel {
            label.text = item.date == nil ? "[No Date]" : dateFormatter.stringFromDate(item.date!)
        }
        // data values
        if let label:UILabel = cell.contentView.viewWithTag(2) as? UILabel {
            label.text = item.dataString
        }
        return cell
    }
    
}