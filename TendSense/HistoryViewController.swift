//
//  HistoryViewController.swift
//  TendSense
//
//  Created by Scott Chalfant on 11/26/15.
//  Copyright © 2015 Tend. All rights reserved.
//

import Foundation
import UIKit
import Charts

class HistoryViewController : UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var ecLineChart: LineChartView!
    @IBOutlet weak var vwcLineChart: LineChartView!
    @IBOutlet weak var tempLineChart: LineChartView!
    @IBOutlet weak var humidityLineChart: LineChartView!
    
    var latitude: Double!
    var longitude: Double!
    
    var region: SensorRegion?
    var items = [SensorDataItem]()
    var dates : [NSDate]!
    override func viewWillAppear(animated: Bool) {

        region = SensorData.instance.regionForLocation(latitude, longitude: longitude)
        if region != nil {
            items = SensorData.instance.itemsForRegion(region!)
            titleLabel.text = NSString(format: "\(region?.name ?? "[Unnamed Region]") (%d samples)", items.count) as String

        } else {
            items = SensorData.instance.historyForLocation(latitude, longitude: longitude, radius: HISTORY_RADIUS)
            titleLabel.text = NSString(format: "%0.4f, %0.4f (%d samples)", latitude, longitude, items.count) as String
        }
        dates = SensorData.instance.datesForItems(items)

        var xLabels = [String]()
        for date in dates {
            xLabels.append(SensorData.instance.dayDateFormatter.stringFromDate(date))
        }

        buildECChart(items, xLabels: xLabels)
        buildVWCChart(items, xLabels: xLabels)
        buildHumidityChart(items, xLabels: xLabels)
        buildTempChart(items, xLabels: xLabels)
        
    }
    
    private func indexOfDate(date: NSDate) -> Int? {
        let s = SensorData.instance.dayDateFormatter.stringFromDate(date)
        var i = 0
        for d in dates {
            if SensorData.instance.dayDateFormatter.stringFromDate(d) == s {
                return i
            }
            i++
        }
        return nil
    }
    
    private func buildVWCChart(items: [SensorDataItem], xLabels: [String]) {
        
        var dataEntries: [ChartDataEntry] = []
        for i in items {
            if let index = indexOfDate(i.date!) {
                dataEntries.append(ChartDataEntry(value: i.vwc!, xIndex: index))
            }
        }
        let lineChartDataSet = LineChartDataSet(yVals: dataEntries, label: "VWC")
        
        
        let lineChartData = LineChartData(xVals: xLabels, dataSet: lineChartDataSet)
        vwcLineChart.data = lineChartData
    }
    private func buildECChart(items: [SensorDataItem], xLabels: [String]) {
        
        var dataEntries: [ChartDataEntry] = []
        for i in 0..<items.count {
            dataEntries.append(ChartDataEntry(value: items[i].ec!, xIndex: i))
        }
        let lineChartDataSet = LineChartDataSet(yVals: dataEntries, label: "EC")
        
        
        let lineChartData = LineChartData(xVals: xLabels, dataSet: lineChartDataSet)
        ecLineChart.data = lineChartData
    }
    private func buildHumidityChart(items: [SensorDataItem], xLabels: [String]) {
        
        var dataEntries: [ChartDataEntry] = []
        for i in 0..<items.count {
            dataEntries.append(ChartDataEntry(value: items[i].humidity!, xIndex: i))
        }
        let lineChartDataSet = LineChartDataSet(yVals: dataEntries, label: "VWC")
        
        
        let lineChartData = LineChartData(xVals: xLabels, dataSet: lineChartDataSet)
        humidityLineChart.data = lineChartData
    }

    private func buildTempChart(items: [SensorDataItem], xLabels: [String]) {
        
        var soilEntries: [ChartDataEntry] = []
        var airEntries: [ChartDataEntry] = []
        for i in 0..<items.count {
            soilEntries.append(ChartDataEntry(value: items[i].soilTemperature!, xIndex: i))
            airEntries.append(ChartDataEntry(value: items[i].airTemperature!, xIndex: i))
        }
        let soilDataSet = LineChartDataSet(yVals: soilEntries, label: "Soil T")
        let airDataSet = LineChartDataSet(yVals: airEntries, label: "Air T")
        
        
        let lineChartData = LineChartData(xVals: xLabels, dataSets: [airDataSet, soilDataSet])
        tempLineChart.data = lineChartData
    }

    
    
}