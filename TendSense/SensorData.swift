//
//  SensorData.swift
//  TendSense
//
//  Created by Scott Chalfant on 11/4/15.
//  Copyright © 2015 Tend. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

let HISTORY_RADIUS = 10.0



let data_EC = 0
let data_VWC = 1
let data_AIRT = 2
let data_SOILT = 3
let data_HUMIDITY = 4

// red, yellow, green
let defaultScaleColors = [ Util.hexToColor("C87D54"), Util.hexToColor("C4AF43"), Util.hexToColor("608149")]

// blue, green, red
let bgrScaleColors = [ Util.hexToColor("635188"), Util.hexToColor("779B3B"), Util.hexToColor("C87D54") ]

// blue purple green yellow red
let tempScaleColors = [ Util.hexToColor("0000FF"), Util.hexToColor("FF00FF"),Util.hexToColor("00FF00"),Util.hexToColor("FFFF00"), Util.hexToColor("FF0000") ]

// red yellow green blue blue
let vwcScaleColors = [ Util.hexToColor("FF0000"), Util.hexToColor("FFFF00"),Util.hexToColor("00FF00"), Util.hexToColor("0000FF"), Util.hexToColor("0000FF")]

// light yellow, light blue to blue
let humidityScaleColors = [ Util.hexToColor("FFFF66"), Util.hexToColor("00FFFF"), Util.hexToColor("0000FF")]

///
/// compute the color for the value
/// :value: between 0.0 and 1.0
/// :scaleColors: 2 or more colors
func _scaleColor(var value: Double, scaleColors: [UIColor]) -> UIColor {
    
    if value < 0.0 { value = 0.0 }
    if value > 1.0 { value = 1.0 }
    
    
    let index = Int( ceil(value * Double(scaleColors.count-1)) )
    if index == 0 {
        return scaleColors[0]
    }
    
    let c1 = scaleColors[index-1]
    let c2 = scaleColors[index]
    let cc1 = CGColorGetComponents(c1.CGColor)
    let cc2 = CGColorGetComponents(c2.CGColor)
    var p : CGFloat = CGFloat(value) - CGFloat(index-1) * (1.0 / CGFloat(scaleColors.count-1))
    p = p * CGFloat(scaleColors.count-1)
    
    return UIColor(red: p*cc2[0] + (1.0-p)*cc1[0],
        green: p*cc2[1] + (1.0-p)*cc1[1],
        blue:  p*cc2[2] + (1.0-p)*cc1[2],
        alpha: p*cc2[3] + (1.0-p)*cc1[3])
    
    
}

///
/// meta data for a kind of sensor data (e.g. VWC)
///
/// colors, ranges, number formats...
class DataType {
    init(name: String, min: Double, max: Double, scaleColors: [UIColor], displayFormat: String, displayFactor: Double = 1.0) {
        self.name = name
        self.min = min
        self.max = max
        self.scaleColors = scaleColors
        self.displayFormat = displayFormat
        self.displayFactor = displayFactor
    }
    
    let name : String
    let min : Double
    let max : Double
    let scaleColors : [UIColor]
    /// multiply by this for display
    let displayFactor : Double
    /// format with this (after multiplying by scale factor)
    let displayFormat : String
    
    ///
    /// normalize based on min/max to a number between 0 and 1.0
    func normalize(value: Double) -> Double {
        var v = value
        if v < min {
            v = min
        }
        if v > max {
            v = max
        }
        return (v-min)/(max-min)
    }
    
    ///
    /// get the gradient color for this unscaled, unnormalized value
    func gradientColor(value: Double) -> UIColor {

        let scaledV = normalize(value)
        
        return _scaleColor(scaledV, scaleColors: scaleColors)
    }
    
    ///
    /// value as formatted string
    func format(value: Double) -> String {
        var v = value
        if v < min {
            v = min
        }
        if v > max {
            v = max
        }
        v *= displayFactor
        
        return NSString(format: displayFormat, v) as String
    }
}

let dataEC  =       DataType(name: "EC",        min: 0.0,   max: 3.0,   scaleColors: bgrScaleColors,      displayFormat: "%0.2fdS/m")
let dataVWC =       DataType(name: "VWC",       min: 0.0,   max: 0.5,   scaleColors: vwcScaleColors,      displayFormat: "%0.1f%%", displayFactor: 100.0)
let dataAirT =      DataType(name: "Air T",     min: -10.0, max: 50.0,  scaleColors: tempScaleColors,     displayFormat: "%0.1fC")
let dataSoilT =     DataType(name: "Soil T",    min: -10.0, max: 50.0,  scaleColors: tempScaleColors,     displayFormat: "%0.1fC")
let dataHumidity =  DataType(name: "Humidity",  min: 0.0,   max: 100.0, scaleColors: humidityScaleColors, displayFormat: "%0.1f%%")

let dataTypes = [dataEC, dataVWC, dataAirT, dataSoilT, dataHumidity]



///
/// singleton that manages storage of sensor data readings and regions
///
/// also provides some operations on the data
class SensorData {
    static var instance = SensorData()
    
    private init() {
        dateFormatter.dateFormat = "yyyy-MM-dd_HH:mm:ss"
        dayDateFormatter.dateFormat = "yyyy-MM-dd"
        
        getDataFile() // force init of test data
        getRegionsFile()
        loadData()
        loadRegions()
    }
    
    var dateFormatter = NSDateFormatter()
    var dayDateFormatter = NSDateFormatter()

    ///
    /// all sensor data readings
    var items = [SensorDataItem]()
    
    ///
    /// all user-defined regions
    var regions = [SensorRegion]()

    
    func clearData() {
        // backup
        syncDataToAWS("dataCSV-backup\(NSDate())")

        items = []
        
        let m = NSFileManager.defaultManager()
        do {
            try m.removeItemAtPath(fileName)
        } catch let e as NSError {
            print("error \(e)")
        }
        dataFile = nil
        loadData()
        syncDataToAWS()
    }
    
    func clearAndLoadSampleData() {
        clearData()
        clearRegions()
        
        createDataFile(true)
        createRegionsFile(true)
        
        loadData()
        loadRegions()
    }
    
    func clearRegions() {
        // backup
        syncRegionsToAWS("regionCSV-backup\(NSDate())")

        items = []
        
        let m = NSFileManager.defaultManager()
        do {
            try m.removeItemAtPath(regionsFileName)
        } catch let e as NSError {
            
        }
        regionsFile = nil
        loadRegions()
        
    }
    
    ///
    /// get all the items for the region 
    /// and optionally constrain to the date
    func itemsForRegion(region: SensorRegion, date: NSDate? = nil) -> [SensorDataItem] {
        let list = date == nil ? items : itemsForDate(date!)
        var res = [SensorDataItem]()
        
        for i in list {
            if region.inside(i) {
                res.append(i)
            }
        }
        
        return res
    }
    
    
    ///
    /// get the region that contains this lat/long
    func regionForLocation(latitude: Double, longitude: Double) -> SensorRegion? {
        for r in regions {
            if r.inside(latitude, longitude: longitude) {
                return r
            }
        }
        return nil
    }
    
    ///
    /// get all the items with no region
    /// and optionally constrain to the date
    func itemsWithoutRegions(date: NSDate? = nil) -> [SensorDataItem] {
        let list = date == nil ? items : itemsForDate(date!)
        var res = [SensorDataItem]()
        
        for i in list {
            
            var inside = false
            for region in regions {
                if region.inside(i) {
                    inside = true
                    break
                }
            }
            if !inside {
                res.append(i)
            }
        }
        
        return res
    }
    
    /// 
    /// get all the sensor data collected on this date (whole day)
    func itemsForDate(date: NSDate) -> [SensorDataItem] {
        let d = dayDateFormatter.stringFromDate(date)
        
        var res = [SensorDataItem]()
        for i in items {
            if let mydate = i.date {
                let id = dayDateFormatter.stringFromDate(mydate)
                if id == d {
                    res.append(i)
                }
            }
        }
        return res
    }
    
    ///
    /// get distinct dates in order that have sensor data
    var dates : [NSDate] {
        get {
            return datesForItems(items)
        }
    }

    func datesForItems(items: [SensorDataItem]) -> [NSDate] {
        var res = [NSDate]()
        for i in items {
            if let mydate = i.date {
                let id = dayDateFormatter.stringFromDate(mydate)
                
                if res.count == 0 || id != dayDateFormatter.stringFromDate(res.last!) {
                    res.append(mydate)
                }
            }
        }
        return res
        
    }
    
    func datesForRegion(region: SensorRegion) -> [NSDate] {
        let items = itemsForRegion(region)
        return datesForItems(items)
    }
    
    ///
    /// get all the sensor date items (across time) that are within
    /// 'radius' meters of the lat/lon position
    func historyForLocation(latitude: Double, longitude: Double, radius: Double) -> [SensorDataItem] {
        let loc = CLLocation(latitude: latitude, longitude: longitude)
        var res = [SensorDataItem]()
        for d in dates {
            var closest : SensorDataItem? = nil
            var closestDistance : Double = 0.0
            for i in itemsForDate(d) {
                let otherLoc = CLLocation(latitude: i.latitude!, longitude: i.longitude!)
                let d = otherLoc.distanceFromLocation(loc)
                if closest == nil || d < closestDistance {
                    closest = i
                    closestDistance = d
                }
            }
            if let c = closest where closestDistance < radius {
                res.append(c)
            }
            
        }
        return res
        
    }
    ///
    /// store the current data in 'sensor'
    func writeCurrentData(sensor: Sensor) {
        if let f = getDataFile() {
            var output = "\(dateFormatter.stringFromDate(NSDate()))"
            output += ",\(sensor.latitude ?? 0.0)"
            output += ",\(sensor.longitude ?? 0.0)"
            output += ",\(sensor.altitude ?? 0.0)"
            output += ",\(sensor.soilTemperature ?? 0.0)"
            output += ",\(sensor.ec ?? 0.0)"
            output += ",\(sensor.vwc ?? 0.0)"
            output += ",\(sensor.airTemperature ?? 0.0)"
            output += ",\(sensor.humidity ?? 0.0)"
            output += "\n"
            
            f.writeData(output.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
            f.synchronizeFile() // bad idea for performance?
            
            // record
            let item = SensorDataItem()
            item.date = sensor.timestamp ?? NSDate()
            item.latitude = sensor.latitude
            item.longitude = sensor.longitude
            item.altitude = sensor.altitude
            item.soilTemperature = sensor.soilTemperature
            item.ec = sensor.ec
            item.vwc = sensor.vwc
            item.airTemperature = sensor.airTemperature
            item.humidity = sensor.humidity
            items.append(item)
            
            syncDataToAWS()
        }
    }
    
    private func syncDataToAWS(id: String = "dataCSV", callback: (()->())? = nil) {

        do {
            let output = try NSString(contentsOfFile: fileName, encoding: NSUTF8StringEncoding) as String
            
            // Initialize the Cognito Sync client
            let syncClient = AWSCognito.defaultCognito()
            
            // Create a record in a dataset and synchronize with the server
            var dataset = syncClient.openOrCreateDataset("data")
            dataset.setString(output as String, forKey: id)
            dataset.synchronize().continueWithBlock {(task: AWSTask!) -> AnyObject! in
                print("data synced to aws")
                callback?()
                // Your handler code here
                return nil
            
            }
        } catch let e as NSError {
            print("error syncing data \(e)")
        }
    
    }
    
    ///
    /// load the store of sensor data
    private func loadData() {
        items = [SensorDataItem]()
        do {
            let data = try NSString(contentsOfFile: fileName, encoding: NSUTF8StringEncoding) as String

            let lines = data.characters.split { $0 == "\n" }
            for line in lines {
                
                let fields = line.split { $0 == "," }
                print("LINE: \(line) | \(fields)")
                
                // skip header row and blank rows
                if fields.count == 0 || String(fields[0]) == "date" {
                    continue
                }
                
                let item = SensorDataItem()
                var i = 0
                item.date = dateFormatter.dateFromString(String(fields[i++]))
                item.latitude = Double(String(fields[i++]))
                item.longitude = Double(String(fields[i++]))
                item.altitude = Double(String(fields[i++]))
                item.soilTemperature = Double(String(fields[i++]))
                item.ec = Double(String(fields[i++]))
                item.vwc = Double(String(fields[i++]))
                item.airTemperature = Double(String(fields[i++]))
                item.humidity = Double(String(fields[i++]))
                
                items.append(item)
            }
        } catch let err as NSError {
            print("error \(err)")
        }
    }
    
    ///
    /// save all the regions here to a local file
    func saveRegions() {

        var output = "name,coords\n" // header
        for r in regions {
            // name
            if let n = r.name {
                let name = n.stringByReplacingOccurrencesOfString(",", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                output += name + ","
            } else {
                output += "Unamed Region,"
            }
            
            // coords
            for c in r.coords {
                output += "\(c.latitude)"
                output += ":"
                output += "\(c.longitude)"
                output += "|"
            }
            
            // done with line
            output += "\n"
        
        }
        
        // write file
        NSFileManager.defaultManager().createFileAtPath(regionsFileName, contents: output.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!, attributes: nil)
        
        syncRegionsToAWS()
    }

    private func syncRegionsToAWS(id: String = "regionCSV", callback: (()->())? = nil) {
        
        do {
            let output = try NSString(contentsOfFile: regionsFileName, encoding: NSUTF8StringEncoding) as String
            
        
            // Initialize the Cognito Sync client
            let syncClient = AWSCognito.defaultCognito()
            
            // Create a record in a dataset and synchronize with the server
            var dataset = syncClient.openOrCreateDataset("regions")
            dataset.setString(output, forKey: id)
            dataset.synchronize().continueWithBlock {(task: AWSTask!) -> AnyObject! in
                print("regions synced to aws")
                // Your handler code here
                return nil
                
            }
        } catch let e as NSError {
            print("Error syncing regions \(e)")
        }
    }
    
    ///
    /// load the store of user-defined regions
    private func loadRegions() {
        regions = [SensorRegion]()
        do {
            let data = try NSString(contentsOfFile: regionsFileName, encoding: NSUTF8StringEncoding) as String
            
            let lines = data.characters.split { $0 == "\n" }
            for line in lines {
                let fields = line.split { $0 == "," }
                print("LINE: \(line) | \(fields)")
                
                // skip header row
                if fields.count == 0 || String(fields[0]) == "name" || String(fields[0]) == "" {
                    continue
                }
                
                let region = SensorRegion()
                var i = 0
                region.name = String(fields[i++])
                
                let coordsString = String(fields[i++])
                let coords = coordsString.characters.split { $0 == "|" }
                for c in coords {
                    let parts = String(c).characters.split { $0 == ":" }
                    let c2d = CLLocationCoordinate2DMake(CLLocationDegrees(String(parts[0]))!,
                        CLLocationDegrees(String(parts[1]))!)
                    region.coords.append(c2d)
                }
                
                
                regions.append(region)
            }
        } catch let err as NSError {
            print("error \(err)")
        }
    }
    
    private var dataFile : NSFileHandle?
    var fileName : String {
        return (self.folderPath! as NSString).stringByAppendingPathComponent("data.csv")
    }
    private func getDataFile() -> NSFileHandle? {
        if (dataFile == nil) {
            
            // create if needed
            if !NSFileManager.defaultManager().fileExistsAtPath(fileName) {
                
                createDataFile()
            }
            
            
            dataFile = NSFileHandle(forUpdatingAtPath: fileName)
            dataFile?.seekToEndOfFile()
        }
        return dataFile
    }

    
    private func createDataFile(sampleData: Bool = false) {
        var output = "date"
        output += ",latitude"
        output += ",longitude"
        output += ",altitude"
        output += ",soilTemperature"
        output += ",ec"
        output += ",vwc"
        output += ",airTemperature"
        output += ",humidity"
        output += "\n"
        
        if sampleData {
            output +=
                
                "2015-11-01_11:00:01,41.7091616528677,-72.3663051407042,100,35,0.7,.35,2,0.45,squash\n" +
                "2015-11-01_11:00:01,41.7094182770947,-72.3659464388442,100,35,0.7,.55,20,0.45,squash\n" +
                "2015-11-01_11:00:01,41.7090891284439,-72.3659090740672,100,35,0.7,.80,25,0.45,squash\n" +
                "\n" +
                "2015-11-01_11:00:01,41.7087962405156,-72.3649114345192,100,35,0.7,.50,25,0.45\n" +
                "2015-11-01_11:00:01,41.7083750374414,-72.3648927521307,100,35,0.7,.70,20,0.45\n" +
                "\n" +
                "2015-11-01_11:00:01,41.7084949829687,-72.3656288382391,100,35,0.7,.20,25,0.45\n" +
                "2015-11-01_11:00:01,41.7082020923336,-72.365617628806,100,35,0.7,.25,25,0.45\n" +
                "\n" +
                "\n" +
                "2015-11-01_11:00:01,41.7082885649456,-72.3659090740672,100,35,0.7,.40,30,0.45\n" +
                "\n" +
                "\n" +
                "2015-11-01_11:00:01,41.7085340349526,-72.3662341476278,100,35,0.7,.20,40,0.45\n" +
                "2015-11-01_11:00:01,41.7082885649456,-72.3661855734176,100,35,0.7,.25,35,0.45\n" +
                "2015-11-01_11:00:01,41.7080403045535,-72.3660510602201,100,35,0.7,.07,30,0.45\n" +
                "\n" +
                "\n" +
                "2015-11-01_11:00:01,41.7084838252547,-72.3664957010673,100,35,0.7,.16,30,0.45\n" +
                "2015-11-01_11:00:01,41.7080542517919,-72.3663985526469,100,35,0.7,.28,25,0.45\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "2015-11-02_11:00:01,41.7091616528677,-72.3663051407042,100,35,0.7,.15,2,0.45,squash\n" +
                "2015-11-02_11:00:01,41.7094182770947,-72.3659464388442,100,35,0.7,.25,20,0.45,squash\n" +
                "2015-11-02_11:00:01,41.7090891284439,-72.3659090740672,100,35,0.7,.30,25,0.45,squash\n" +
                "\n" +
                "2015-11-02_11:00:01,41.7087962405156,-72.3649114345192,100,35,0.7,.20,25,0.45\n" +
                "2015-11-02_11:00:01,41.7083750374414,-72.3648927521307,100,35,0.7,.15,20,0.45\n" +
                "\n" +
                "2015-11-02_11:00:01,41.7084949829687,-72.3656288382391,100,35,0.7,.05,25,0.45\n" +
                "2015-11-02_11:00:01,41.7082020923336,-72.365617628806,100,35,0.7,7,.25,0.45\n" +
                "\n" +
                "\n" +
                "2015-11-02_11:00:01,41.7082885649456,-72.3659090740672,100,35,0.7,.20,30,0.45\n" +
                "\n" +
                "\n" +
                "2015-11-02_11:00:01,41.7085340349526,-72.3662341476278,100,35,0.7,.20,40,0.45\n" +
                "2015-11-02_11:00:01,41.7082885649456,-72.3661855734176,100,35,0.7,.22,35,0.45\n" +
                "2015-11-02_11:00:01,41.7080403045535,-72.3660510602201,100,35,0.7,.05,30,0.45\n" +
                "\n" +
                "\n" +
                "2015-11-02_11:00:01,41.7084838252547,-72.3664957010673,100,35,0.7,.08,30,0.45\n" +
                "2015-11-02_11:00:01,41.7080542517919,-72.3663985526469,100,35,0.7,.17,25,0.45\n" +
                "\n" +
                "\n" +
                "2015-11-02_11:01:01,41.709365278697,-72.3652439810352,100,35,0.7,.20,30,0.45\n" +
                "2015-11-02_11:01:01,41.7079315159896,-72.3647881307548,100,35,0.7,.05,30,0.45\n" +
                "2015-11-02_11:01:01,41.709393172596,-72.3650197923727,100,35,0.7,.20,30,0.45\n" +
                "\n" +
                "\n" +
                "2015-11-03_11:00:01,41.7091616528677,-72.3663051407042,100,35,0.7,.17,2,0.45,squash\n" +
                "2015-11-03_11:00:01,41.7094182770947,-72.3659464388442,100,35,0.7,.25,20,0.45,squash\n" +
                "2015-11-03_11:00:01,41.7090891284439,-72.3659090740672,100,35,0.7,.33,25,0.45,squash\n" +
                "\n" +
                "2015-11-03_11:00:01,41.7087962405156,-72.3649114345192,100,35,0.7,.25,25,0.45\n" +
                "2015-11-03_11:00:01,41.7083750374414,-72.3648927521307,100,35,0.7,.31,20,0.45\n" +
                "\n" +
                "2015-11-03_11:00:01,41.7084949829687,-72.3656288382391,100,35,0.7,.10,25,0.45\n" +
                "2015-11-03_11:00:01,41.7082020923336,-72.365617628806,100,35,0.7,.10,25,0.45\n" +
                "\n" +
                "\n" +
                "2015-11-03_11:00:01,41.7082885649456,-72.3659090740672,100,35,0.7,.25,30,0.45\n" +
                "\n" +
                "\n" +
                "2015-11-03_11:00:01,41.7085340349526,-72.3662341476278,100,35,0.7,.20,40,0.45\n" +
                "2015-11-03_11:00:01,41.7082885649456,-72.3661855734176,100,35,0.7,.25,35,0.45\n" +
                "2015-11-03_11:00:01,41.7080403045535,-72.3660510602201,100,35,0.7,.05,30,0.45\n" +
                "\n" +
                "\n" +
                "2015-11-03_11:00:01,41.7084838252547,-72.3664957010673,100,35,0.7,.15,30,0.45\n" +
                "2015-11-03_11:00:01,41.7080542517919,-72.3663985526469,100,35,0.7,.25,25,0.45\n" +
                "\n" +
                "\n" +
                "2015-11-03_11:01:01,41.7090947072486,-72.3652028797804,100,35,0.7,.25,30,0.45\n" +
                "2015-11-03_11:01:01,41.7078617796339,-72.3655167439079,100,35,0.7,.45,30,0.45\n" +
                "2015-11-03_11:01:01,41.7081351457153,-72.3667161532521,100,35,0.7,.15,30,0.45\n" +
                "2015-11-03_11:01:01,41.7082913543828,-72.3667348356406,100,35,0.7,.10,30,0.45\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "2015-11-04_11:00:01,41.7091616528677,-72.3663051407042,100,35,0.7,.15,2,0.45,squash\n" +
                "2015-11-04_11:00:01,41.7094182770947,-72.3659464388442,100,35,0.7,.40,20,0.45,squash\n" +
                "2015-11-04_11:00:01,41.7090891284439,-72.3659090740672,100,35,0.7,.15,25,0.45,squash\n" +
                "\n" +
                "2015-11-04_11:00:01,41.7087962405156,-72.3649114345192,100,35,0.7,.10,25,0.45\n" +
                "2015-11-04_11:00:01,41.7083750374414,-72.3648927521307,100,35,0.7,.25,20,0.45\n" +
                "\n" +
                "2015-11-04_11:00:01,41.7084949829687,-72.3656288382391,100,35,0.7,.05,25,0.45\n" +
                "2015-11-04_11:00:01,41.7082020923336,-72.365617628806,100,35,0.7,.07,25,0.45\n" +
                "\n" +
                "\n" +
                "2015-11-04_11:00:01,41.7082885649456,-72.3659090740672,100,35,0.7,.30,30,0.45\n" +
                "\n" +
                "\n" +
                "2015-11-04_11:00:01,41.7085340349526,-72.3662341476278,100,35,0.7,.15,40,0.45\n" +
                "2015-11-04_11:00:01,41.7082885649456,-72.3661855734176,100,35,0.7,.22,35,0.45\n" +
                "2015-11-04_11:00:01,41.7080403045535,-72.3660510602201,100,35,0.7,.16,30,0.45\n" +
                "\n" +
                "\n" +
                "2015-11-04_11:00:01,41.7084838252547,-72.3664957010673,100,35,0.7,.15,30,0.45\n" +
                "2015-11-04_11:00:01,41.7080542517919,-72.3663985526469,100,35,0.7,.25,25,0.45\n" +
                "\n" +
                "\n" +
                "\n" +
                "2015-11-04_11:02:01,41.709365278697,-72.3652439810352,100,35,0.7,.15,30,0.45\n" +
                "2015-11-04_11:02:01,41.7090947072486,-72.3652028797804,100,35,0.7,.20,30,0.45\n" +
                "2015-11-04_11:02:01,41.7079315159896,-72.3647881307548,100,35,0.7,.25,30,0.45\n" +
                "2015-11-04_11:02:01,41.7078617796339,-72.3655167439079,100,35,0.7,.40,30,0.45\n" +
                "2015-11-04_11:02:01,41.7081351457153,-72.3667161532521,100,35,0.7,.10,30,0.45\n" +
                "2015-11-04_11:02:01,41.7082913543828,-72.3667348356406,100,35,0.7,.15,30,0.45\n" +
                "2015-11-04_11:02:01,41.709393172596,-72.3650197923727,100,35,0.7,.05,30,0.45\n" +
                "\n" +
            "\n"
        }
        NSFileManager.defaultManager().createFileAtPath(fileName, contents: output.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!, attributes: nil)
    }
    
    
    private var regionsFile : NSFileHandle?
    var regionsFileName : String {
        return (self.folderPath! as NSString).stringByAppendingPathComponent("regions.csv")
    }
    func getRegionsFile() -> NSFileHandle? {
        if (regionsFile == nil) {
            
            // create if needed
            if !NSFileManager.defaultManager().fileExistsAtPath(regionsFileName) {
                    createRegionsFile()
            }
            
            
            regionsFile = NSFileHandle(forUpdatingAtPath: regionsFileName)
            regionsFile?.seekToEndOfFile()
        }
        return regionsFile
    }
    
    
    private func createRegionsFile(sampleData: Bool = false) {
        var output = "name"
        output += ",coords"
        output += "\n"
        if sampleData {
            output +=
                
                "tomatoes,41.7085269935342:-72.3666625970373|41.70824877024:-72.3665040675627|41.7081304211603:-72.3665346609701|41.7078708829419:-72.3664567868422|41.7079435537487:-72.3662426329905|41.7086058924582:-72.3664206309971|\n" +
                "peas,41.7085913584531:-72.3663650066201|41.7079394011334:-72.3662009147078|41.7079601642072:-72.3660562913274|41.7080307586083:-72.3659756359807|41.7086432655989:-72.3661647588627|\n" +
                "carrots,41.7080286823035:-72.3659144491659|41.7080826662067:-72.3657865130987|41.7085498327064:-72.3658727308831|41.7084854677458:-72.3660535101085|\n" +
                "corn,41.7080639794761:-72.3657587009101|41.7081241922553:-72.365477797806|41.7083795768641:-72.3655612343716|41.708454323387:-72.3653915800216|41.7086474181688:-72.3655250785265|41.7086204264599:-72.3657225450651|41.7085269935342:-72.3658393562569|\n" +
                "squash,41.7087926053676:-72.3660629434993|41.7088323670325:-72.3657524775423|41.7094903000523:-72.3656754859381|41.70952635468:-72.3659436288981|41.7094036331901:-72.3662976553826|41.7090970550745:-72.3664928195335|41.7089205645859:-72.3665156067446|41.7090286163023:-72.3660999029451|\n" +
            "peaches,41.7081138338064:-72.3652619980054|41.7085607799852:-72.3650911710388|41.7087823430718:-72.3652493402451|41.7089001755057:-72.3648747546556|41.7082745830576:-72.364669808995|"
        }
        NSFileManager.defaultManager().createFileAtPath(regionsFileName, contents: output.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!, attributes: nil)
        
    }
    
    private var folderPath : String? {
        let fm : NSFileManager = NSFileManager.defaultManager()
        
        let documentsDirectory = (NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true).first! )
        
        let outputPath = (documentsDirectory as NSString).stringByAppendingPathComponent("SensorData")
        
        if !fm.fileExistsAtPath(outputPath) {
            do {
                try fm.createDirectoryAtPath(outputPath, withIntermediateDirectories: true, attributes: nil)
            } catch let error {
                print("Could not create outputPath directory: \(error)");
                return nil
            }
        }
        return outputPath
    }

}

///
/// a sensor measurement with a date and fields for each reading
class SensorDataItem {
    
    var date: NSDate?
    
    // data elements
    var airTemperature: Double? = nil // deg C
    var soilTemperature: Double? = nil // deg C
    var humidity: Double? = nil    // percent
    
    // location
    var latitude: Double? = nil
    var longitude: Double? = nil
    var altitude: Double? = nil
    
    
    var hasCoordinate : Bool {
        return latitude != nil && longitude != nil && latitude! != 0.0 && longitude! != 0.0
    }
    
    // soil
    var ec: Double? = nil
    var vwc: Double? = nil

    ///
    /// format a nice-ish string out of the data
    var dataString : String {

        var res = ""
        for var i = 0; i < dataTypes.count; i++ {
            if res.characters.count > 0 {
                res += ", "
            }
            // format value
            let s = dataTypes[i].format(self[i])
            res += "\(dataTypes[i].name)=\(s)"
        }
        
        return res
        
    }
    
    ///
    /// get data value by index... see data_* constants
    subscript(dataIndex: Int) -> Double {

        if dataIndex == data_EC, let t = ec {
            return t
        }
        if dataIndex == data_VWC, let t = vwc {
            return t
        }
        if dataIndex == data_AIRT, let t = airTemperature {
            return t
        }
        if dataIndex == data_SOILT, let t = soilTemperature {
            return t
        }
        if dataIndex == data_HUMIDITY, let t = humidity {
            return t
        }
        return 0.0
        
        
    }
    
}

///
/// a polygon region - like a field or a bed
class SensorRegion {
    var name: String?
    var coords = [CLLocationCoordinate2D]()

    ///
    /// is this point inside?
    func inside(loc: CLLocationCoordinate2D) -> Bool {
        let poly = MKPolygon(coordinates: &coords, count: coords.count)
        return poly.pointInside(loc)
    }
    func inside(item: SensorDataItem) -> Bool {
        if let lat = item.latitude, lon = item.longitude {
            return inside(lat, longitude: lon)
        } else {
            return false
        }
    }
    func inside(latitude: Double, longitude: Double) -> Bool {
        let loc = CLLocationCoordinate2DMake(latitude, longitude)
        let poly = MKPolygon(coordinates: &coords, count: coords.count)
        return poly.pointInside(loc)
    }
}


