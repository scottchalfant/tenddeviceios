
#ifndef DATETIME_H
#define DATETIME_H

#import <AWSCore/AWSCore.h>
#import <AWSCognito/AWSCognito.h>

// date time structure shared with arduino code
typedef struct  {
    unsigned int year   : 5; // (20) 00-31
    unsigned int month  : 4; // 1-12
    unsigned int day    : 5; // 1-31
    unsigned int hour   : 5; // 0-23
    unsigned int minute : 6; // 0-59
    unsigned int seconds: 6; // 0-59
} DateTime; // 4 bytes



#endif //DATETIME_H