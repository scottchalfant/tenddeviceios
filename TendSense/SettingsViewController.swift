//
//  SettingsViewController.swift
//  TendSense
//
//  Created by Scott Chalfant on 11/4/15.
//  Copyright © 2015 Tend. All rights reserved.
//

import Foundation
import UIKit

class SettingsViewController : UITableViewController {
    
    @IBOutlet weak var currentSensorLabel: UILabel!
 
    @IBOutlet weak var currentRegtionsLabel: UILabel!
    
    
    @IBOutlet weak var dataCountLabel: UILabel!
    override func viewDidAppear(animated: Bool) {
        refresh()
    }
    func refresh() {
        // current sensor connection
        if let last = SensorManager.instance.lastSensor  {
            currentSensorLabel.text = "Current: \(last.name ?? "Unknown")"
        }

        // count of current regions
        let c = SensorData.instance.regions.count
        if c == 0 {
            currentRegtionsLabel.text = "None."
        } else if c == 1 {
            currentRegtionsLabel.text = "One region defined."
        } else  {
            currentRegtionsLabel.text = "\(c) regions defined."
        }
        
        if SensorData.instance.items.count ==  0{
            dataCountLabel.text = "No data."
        } else if SensorData.instance.items.count == 1 {
                dataCountLabel.text = "1 sample."
        } else {
            dataCountLabel.text = "\(SensorData.instance.items.count) samples."
        }
        
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 2 { // clear data
            Util.alert(self, title: "Clear Data?", message: "Are you sure you want to clear all sensor data?  Note that a backup will be stored on the server.", buttons: ["Yes", "No"], result: { (buttonPressed) -> () in
                if buttonPressed == "Yes" {
                    SensorData.instance.clearData()
                    self.refresh()
                }
            })
            
        } else if indexPath.row == 3 { // clear data and load sample
            Util.alert(self, title: "Load Sample Data?", message: "Are you sure you want to clear all sensor data and regions and load sample data? Note that a backup will be stored on the server.", buttons: ["Yes", "No"], result: { (buttonPressed) -> () in
                if buttonPressed == "Yes" {
                    SensorData.instance.clearAndLoadSampleData()
                    self.refresh()
                }
            })
            
        }
    }
    
}