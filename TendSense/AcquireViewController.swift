//
//  AcquireViewController.swift
//  TendSense
//
//  Created by Scott Chalfant on 11/4/15.
//  Copyright © 2015 Tend. All rights reserved.
//

import Foundation



import Foundation
import UIKit

class AcquireViewController : UIViewController, SensorManagerListener, SensorListener {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var recordButton: UIButton!

    var sensorDataVC : SensorDataViewController?
    
    override func viewWillAppear(animated: Bool) {

        recordButton.hidden = true // not used for now
        
        SensorManager.instance.listeners.append(self)
        
        if SensorManager.instance.ready {
            start()
        } else {
            notConnected()
//            statusLabel.text = "Waiting for Sensor..."
//            activityIndicator.hidden = false
//            activityIndicator.startAnimating()
//            recordButton.enabled = false
        }
    }
    func start() {
        print("Sensor manager ready, starting...")
        if let s = SensorManager.instance.lastSensor {
            if s.connected {
                connected()
            } else {
                notConnected()
            }
        } else {
            notConnected()
        }
        refreshData()
    }
    
    
    func refreshData() {
        if let vc = sensorDataVC {
            
            if let s = SensorManager.instance.lastSensor {
                vc.barEC.value = s.ec
                vc.barVWC.value = s.vwc
                vc.barAirTemp.value = s.airTemperature
                vc.barSoilTemp.value = s.soilTemperature
                vc.barHumidity.value = s.humidity

            } else {
                vc.barEC.value = 0.0
                vc.barVWC.value = 0.0
                vc.barAirTemp.value = 0.0
                vc.barSoilTemp.value = 0.0
                vc.barHumidity.value = 0.0

            }
        }
    }
    
    
    func showConnectedStatus() {
        statusLabel.text = "Connected to '\(SensorManager.instance.lastSensor!.name ?? "Unknown")'"
    }
    
    func connected() {
        activityIndicator.stopAnimating()
        activityIndicator.hidden = true
        
        showConnectedStatus()
        recordButton.enabled = true
        sensorDataVC?.tableView.hidden = false
    }
    func notConnected() {
        recordButton.enabled = false
        if let s = SensorManager.instance.lastSensor {
            activityIndicator.hidden = false
            activityIndicator.startAnimating()
            statusLabel.text = "Connecting to '\(s.name ?? "Unknown")'"
            s.listeners.append(self)
            s.connect()
        } else {
            activityIndicator.stopAnimating()
            activityIndicator.hidden = true
            statusLabel.text = "No sensor selected."
        }
        sensorDataVC?.tableView.hidden = true
    }
    
    @IBAction func recordButtonTouched(sender: AnyObject) {
        
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "embedSensorData" {
            if let vc = segue.destinationViewController as? SensorDataViewController {
                sensorDataVC = vc
            }
        }
    }
    
    
    
    func sensorConnectStatus(sensor: Sensor, connected: Bool) {
        if connected {
            self.connected()
        } else {
            notConnected()
        }
    }
    func sensorError(sensor: Sensor, message: String) {
        
        print("ERROR: \(message)")
    }
    func sensorReadComplete(sensor: Sensor) {
        if (bulkTotal < 0) {
            activityIndicator.hidden = true
            activityIndicator.stopAnimating()
            showConnectedStatus() // revert status label
            bulkCount++
            refreshData()
        } else {
            dispatch_async(dispatch_get_main_queue(), {
                if self.bulkTotal > 0 {
                    self.statusLabel.text = "Downloading... (\(self.bulkCount) of \(self.bulkTotal))"
                }
            })
        }
    }
    
    func sensorDataUpdated(sensor: Sensor) {
        refreshData()
    }
    
    func sensorReadStarting(sensor: Sensor) {
        if (bulkTotal < 0) {
            activityIndicator.hidden = false
            activityIndicator.startAnimating()
            statusLabel.text = "Receiving Sensor Data"
        }
    }
    
    var bulkTotal = -1
    var bulkCount = -1
    
    func sensorBatchTransferStarting(sensor: Sensor, count: Int) {
        bulkCount = 1
        bulkTotal = count
        activityIndicator.hidden = false
        activityIndicator.startAnimating()
        statusLabel.text = "Downloading... (\(count) entries)"
    }
    
    func sensorBatchTransferComplete(sensor: Sensor) {
        activityIndicator.hidden = true
        activityIndicator.stopAnimating()
        
        statusLabel.text = "Download Complete"
        NSTimer.schedule(4.0, f: { () -> () in
            self.showConnectedStatus() // revert status label
        })
        bulkCount = -1
        bulkTotal = -1
    }
    
    func sensorManagerReady() {
        start()
    }
    
    func sensorManagerError(message: String) {
        
        
    }
    
    func sensorManagerFoundSensor(sensor: Sensor) {
        
        
    }
    
    
}