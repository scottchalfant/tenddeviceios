//
//  TendMapView.swift
//  TendSense
//
//  Created by Scott Chalfant on 1/3/16.
//  Copyright © 2016 Tend. All rights reserved.
//

import Foundation
import UIKit
import MapKit




///
/// map view with some polygon editing for regions
class TendMapView : MKMapView, MKMapViewDelegate{
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    // additional zoom factor on the map
    var mapZoom = CGFloat(1.0) {
        didSet {
            updateMagnification()
        }
    }
    
    ///
    /// if set, regions will be rendered with data for this date
    var showDataForDate : NSDate? {
        didSet {
            // redraw regions
            refreshRegions()
        }
    }
    var showDataForDataIndex : Int? {
        didSet {
            // redraw regions
            refreshRegions()
        }
    }
    
    ///
    /// use this for the delegate instead of the normal 'delegate' property
    var tendDelegate : MKMapViewDelegate? = nil

    ///
    /// are the polygons on this map editable
    var editable : Bool = false {
        didSet {
            drawingView?.hidden = !editable
            toolbar?.hidden = !editable
            layoutIfNeeded()
        }
    }
    
    ///
    /// constructor helper
    private func initialize() {
        self.delegate = self
        updateMapRegions()
        self.mapType = .Satellite
        self.showsUserLocation = true

        NSTimer.schedule(1.0, f: { () -> () in
            // if we are in the sim, center the map now.  otherwise wait for user location
            if Platform.isSimulator {
                self.centerMap()
            }
        })
    }
    
    var viewController: UIViewController!
    var toolbar : MapToolbar!
    var drawingView : DrawingView!
    var toggleEditButton : UIButton!
    
    
    
    // the region we are editing if any
    var editingRegion: SensorRegion?
    private var editingNew = false
    
    private var editingVertex : Int? = nil
    
    // button titles
    
    let bDone = "Done"
    let bAdd = "New"
    let bCancel = "Cancel"
    let bDelete = "Delete"
    let bRename = "Rename"
    let defaultToolbarText = "Touch a region to edit."
    
    private func modeNoneSelected() {
        editingRegion = nil
        editingVertex = nil
        
        toolbar.text = defaultToolbarText
        self.toolbar.actions = [bAdd]
    }
    private func modeRegionSelected(region: SensorRegion) {
        editingRegion = region
        editingVertex = nil
        self.editingNew = false
        
        self.toolbar.text = "Editing '\(region.name!)' - tap to add points."
        self.toolbar.actions = [bRename, bDelete]
    }
    private func modeNewRegion() {
        ask("Add Region", text: "Please name your new region.", placeHolder: "e.g. 'North Field' or 'Tomatoes'", callback: { (text) -> () in
            
            if let t = text {
                self.editingRegion = SensorRegion()
                self.toolbar.text = "Editing '\(t)' - tap to add points."
                self.editingRegion!.name = t
                SensorData.instance.regions.append(self.editingRegion!)
                self.updateMapRegions()
                self.toolbar.actions = [self.bRename, self.bDelete]
                self.editingNew = true
            }
        })
    }
    
    ///
    /// handle a toolbar button click
    func toolbarAction(action: String) {
        print("tool bar button: \(action)")
        
        if action == bAdd { // add new region
            modeNewRegion()
        } else if action == bCancel { // cancel add
            if let _ = editingRegion {
                SensorData.instance.regions.removeAtIndex(SensorData.instance.regions.count-1)
                modeNoneSelected()
            }
        } else if action == bDelete { // delete current editing region
            if let r = editingRegion {
                var i = 0
                for region in SensorData.instance.regions {
                    if region === r {
                        SensorData.instance.regions.removeAtIndex(i)
                        break
                    }
                    i++
                }
            }
            modeNoneSelected()
        } else if action == bRename {
            if let r = editingRegion {
                ask("Rename Region", text: "Enter new name for \(r.name!):", placeHolder: "e.g. 'North Field' or 'Tomatoes'", callback: { (text) -> () in
                    if let t = text {
                        r.name = t
                        self.toolbar.text = "Editing '\(t)' - tap to add points."
                    }
                })
            }
        }
        
        // update display
        updateMapRegions()
    }
    
    ///
    /// ask for a string value
    private func ask(title: String, text: String, placeHolder: String, callback: (text: String?)->()) {
        let alert = UIAlertController(title: title, message: text, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            callback(text: alert.textFields![0].text)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: { (action) -> Void in
            callback(text: nil)
        }))
        
        alert.addTextFieldWithConfigurationHandler({(textField: UITextField!) in
            textField.placeholder = placeHolder
        })
        
        self.viewController.presentViewController(alert, animated: true) { () -> Void in
        }
    }
    
    ///
    /// handle a new point on the map
    func handleTouch(point: CGPoint) {
        print("touch \(point)")
        
        // already editing a region
        if let r = editingRegion {
            if let v = editingVertex { // editing a vertex
                let loc = convertPoint(point, toCoordinateFromView: self)
                r.coords[v] = loc
                editingVertex = nil
            }
            else if let v = vertexForPoint(point) { // selecting a point
                if editingNew && v == 0 { // touched the start new when editing a new poly... close the polygon
                    editingNew = false
                    updateMapRegions()
                } else if !editingNew { // when building the poly for the first time, no vertex selection
                    editingVertex = v
                }
            } else { // adding a ppint
                
                let loc = convertPoint(point, toCoordinateFromView: self)
                if let index = indexForVertexInsert(point) {
                    r.coords.insert(loc, atIndex: index)
                } else { // try to select a different region
                    if let r = regionForPoint(point) {
                        modeRegionSelected(r)
                    }
                }
                
            }
            updateMapRegions()
        } else { // none selected, try to select new
            if let r = regionForPoint(point) {
                modeRegionSelected(r)
                updateMapRegions()
            }
        }
        
    }
    
    
    
    
    func distToLine(lp1: MKMapPoint, lp2: MKMapPoint, p: MKMapPoint) -> Double {
        // from https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line#Line_defined_by_two_points
        var n = (lp2.y - lp1.y)*p.x  - (lp2.x - lp1.x)*p.y + lp2.x*lp1.y - lp2.y*lp1.x
        n = abs(n)
        let dy = lp2.y - lp1.y
        let dx = lp2.x - lp1.x
        let d = sqrt( dy*dy + dx*dx )
        return n / d
    }
    
    func handleMove(point: CGPoint) {
        print("move \(point)")
        
        if let r = editingRegion {
            if let v = editingVertex {
                let loc = convertPoint(point, toCoordinateFromView: self)
                r.coords[v] = loc
                updateMapRegions()
            }
        }
    }
    
    ///
    /// when the user touches this point, should it lead to insertion of
    /// a vertex?  If yes, return the index of the point in the coord list
    /// of the current editing region that we should insert at
    private func indexForVertexInsert(point: CGPoint) -> Int? {
        if let r = editingRegion {
            if r.coords.count < 3 || editingNew {
                return r.coords.count // insert at the end
            } else {
                let loc = convertPoint(point, toCoordinateFromView: self)
                
                var minI = -1
                var minDist = 0.0
                for var i = 0; i < r.coords.count; i++ {
                    let c1 = i == 0 ? r.coords.last! : r.coords[i-1]
                    let c2 = r.coords[i]
                    
                    let point1 = MKMapPointForCoordinate(c1)
                    let point2 = MKMapPointForCoordinate(c2)
                    let point  = MKMapPointForCoordinate(loc)
                    let distance = distToLine(point1, lp2: point2, p: point)
                    let d1 = MKMetersBetweenMapPoints(point1, point)
                    let d2 = MKMetersBetweenMapPoints(point2, point)
                    let d = MKMetersBetweenMapPoints(point1, point2)
                    
                    if (minI == -1 || distance < minDist) && d1<d && d2<d {
                        minI = i
                        minDist = distance
                    }
                }
                if minI >= 0 {
                    // add between the closest
                    return minI
                } else { // try to select a different region
                    return nil
                }
            }
        }
        return nil
    }
    
    
    ///
    /// returns true of a touch to this point will be handled as a part of
    /// the poly drawing.  otherwise, it will fall through to the map itself for
    /// panning, zooming, etc.
    func handlePointInside(point: CGPoint) -> Bool {
        if editingRegion == nil {
            // grab touches that are on a region
            if let _ = regionForPoint(point) {
                return true
            } else {
                return false
            }
            
        } else {
            // selecting a point?
            if let _ = vertexForPoint(point) {
                return true
            } else {
                
                // would lead to a new vertex?
                if let _ = indexForVertexInsert(point) {
                    return true
                } else {
                    // should this touch lead to selection of another region?
                    if let _ = regionForPoint(point) {
                        return true
                    } else {
                        modeNoneSelected()
                        updateMapRegions()
                        return false
                    }
                }
            }
        }
    }
    
    ///
    /// public method to redraw the map regions - useful if they are edited
    /// elsewhere
    func refreshRegions() {
        updateMapRegions()
    }
    
    ///
    /// internal version of addOverlay that tracks a list of overlays belonging to this class
    private func addMyOverlay(overlay: MKOverlay) {
        myOverlays.append(overlay)
        addOverlay(overlay)
    }
    
    ///
    /// update the display of overlays... create a polygon region for each
    private func updateMapRegions() {
        
        self.removeOverlays(self.overlays)
        myOverlays.removeAll()

        for r in SensorData.instance.regions {
            if r === editingRegion && editingNew {
                let o = MKPolyline(coordinates: &r.coords, count: r.coords.count)
                self.addMyOverlay(o)
            } else {
                let o = MKPolygon(coordinates: &r.coords, count: r.coords.count)
                self.addMyOverlay(o)
                
            }
        }
        
        let radius = Double(2.0 / mapZoom)
        if let r = editingRegion {
            
            var i = 0
            for c in r.coords {
                let circle = MKCircle(centerCoordinate: c, radius: radius)
                circle.title = "\(i)"
                self.addMyOverlay(circle)
                i++
            }
            if let v = editingVertex {
                let circle = MKCircle(centerCoordinate: r.coords[v], radius: radius*3.0)
                circle.title = "\(v)"
                self.addMyOverlay(circle)
                i++
            }
            // some extra circles to indicate the start node
            if editingNew && r.coords.count > 0 {
                let circle = MKCircle(centerCoordinate: r.coords[0], radius: radius*2.0)
                circle.title = "-1"//?
                self.addMyOverlay(circle)
                let circle2 = MKCircle(centerCoordinate: r.coords[0], radius: radius*2.5)
                circle2.title = "-2"//?
                self.addMyOverlay(circle2)
                i++
            }
            
        }
    }
    
    ///
    /// the region under this point
    private func regionForPoint(point: CGPoint) -> SensorRegion? {
        
        var i = 0
        for o in overlays { // note that some overlays are not polygons
            if let p = o as? MKMultiPoint {
                
                let coord = convertPoint(point, toCoordinateFromView: self)
                
                if p.pointInside(coord) {
                    return SensorData.instance.regions[i]
                }
                i++
            }
        }
        return nil
    }
    
    ///
    /// the index of the vertex of the region being edited at this point
    private func vertexForPoint(point: CGPoint) -> Int? {
        let radius = CGFloat(25.0)
        
        if let r = editingRegion {
            var i = 0
            for c in r.coords {
                
                let p = convertCoordinate(c, toPointToView: self) // convert to point
                
                let distance = dist(p, p2: point)
                
                if distance <= radius {
                    return i
                }
                i++
            }
            return nil
            
        } else {
            return nil
        }
        
    }
    
    private func dist(p1: CGPoint, p2: CGPoint) -> CGFloat {
        let xDist = (p1.x - p2.x)
        let yDist = (p1.y - p2.y)
        return sqrt((xDist * xDist) + (yDist * yDist))
    }
    
    private func createSubviewsIfNeeded() {
        if drawingView == nil {
            drawingView = DrawingView()
            
            drawingView.hidden = !editable
            drawingView.addTouch = handleTouch
            drawingView.moveTouch = handleMove
            drawingView.pointInside = handlePointInside
            
            addSubview(drawingView)
        }
        
        if toolbar == nil {
            toolbar = MapToolbar()
            toolbar?.backgroundColor = UIColor(red: 196.0/255.0, green: 118.0/255.0, blue: 69.0/255.0, alpha: 0.5)
            toolbar.actions = [bAdd]
            toolbar.buttonPressed = toolbarAction
            toolbar.hidden = !editable
            superview!.addSubview(toolbar)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // go go go
        createSubviewsIfNeeded()
        
        // layout toolbar
        let h = CGFloat(70.0)
        let pad = CGFloat(10.0)
        
        toolbar.frame = CGRectMake(pad, frame.size.height-pad-h, frame.size.width-pad-pad, h)
        toolbar.layoutSubviews() //pass it on
        // layout drawing view
        drawingView.frame = self.frame
    }
    
    // MARK: Map Delegate
    
    private var myOverlays = [MKOverlay]()
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        
        /// should pass to external delegate
        if tendDelegate != nil && tendDelegate!.respondsToSelector(Selector("mapView:rendererForOverlay:")) && !myOverlays.contains( {$0 === overlay} ) {
            return tendDelegate!.mapView!(mapView, rendererForOverlay: overlay)
        }
        
        
        if overlay is MKMultiPoint {
            
            // polygons are for already existing regions
            if overlay is MKPolygon {
                if let region = regionForOverlay(overlay) {
                    let rr = RegionRenderer(mapView: mapView, overlay: overlay, region: region)
                    rr.date = showDataForDate
                    rr.dataIndex = self.showDataForDataIndex
                    
                    if let er = editingRegion { // if we are editing a region
                        if region === er {
                            rr.editing = true
                        } else if editingNew { // turn the other ones gray when a new region is being drawn
                            rr.active = false
                        } else { // else active and not editing
                            rr.active = true
                            rr.editing = false
                        }
                    }
                
                    return rr
                } else {
                    print("error no region for overlay \(overlay)")
                    return MKPolygonRenderer(overlay: overlay)
                }
            } else { // multi line - for a brand new (unclosed) region
                let r = MKPolylineRenderer(overlay: overlay)
                r.strokeColor = Util.colorBrown
                r.fillColor = nil
                r.lineWidth = 2.0
                return r
            }
        }
        
        // else circle
        let r = MKCircleRenderer(overlay: overlay)
        r.strokeColor = Util.colorOrange
        r.lineWidth = 2.0
        r.fillColor = Util.colorOrange
        if let v = editingVertex, t = overlay.title where v == Int(t!)! {
            r.strokeColor = Util.colorOrange
            r.fillColor = Util.colorOrange.colorWithAlphaComponent(0.5)
        }
        if let t = overlay.title where t == "-1" || t == "-2" {
            r.strokeColor = Util.colorOrange
            r.fillColor = Util.colorOrange.colorWithAlphaComponent(0.5)
        }
        return r
    }
    
    
    
    
    ///
    /// find the sensor data region that goes with this map overlay
    private func regionForOverlay(overlay: MKOverlay) -> SensorRegion? {
        var i = 0
        for o in overlays {
            if o is MKMultiPoint {
                if o === overlay {
                    return SensorData.instance.regions[i]
                }
                i++
            }
        }
        return nil
    }
    
    /// have we set the initial region yet?
    private var firstMapUpdate = true
    
    ///
    /// this zooms in on the user's initial location allows the map to follow the user....
    func mapView(mapView: MKMapView, didUpdateUserLocation userLocation: MKUserLocation) {
        centerMap()
    }
    
        
    private func centerMap() {
        if firstMapUpdate {
            
            // if no regions, center on user
            if SensorData.instance.regions.count == 0 {
                if !Platform.isSimulator {
                    let region:MKCoordinateRegion = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800)
                    self.setRegion(self.regionThatFits(region),animated: true)
                } // else no options?
            } else { // center on regions
                var minLat = 0.0
                var minLon = 0.0
                var maxLat = 0.0
                var maxLon = 0.0
                
                for r in SensorData.instance.regions {
                    for c in r.coords {
                        if minLat == 0.0 || c.latitude < minLat {
                            minLat = c.latitude
                        }
                        if minLon == 0.0 || c.longitude < minLon {
                            minLon = c.longitude
                        }
                        if maxLat == 0.0 || c.latitude > maxLat {
                            maxLat = c.latitude
                        }
                        if maxLon == 0.0 || c.longitude > maxLon {
                            maxLon = c.longitude
                        }
                    }
                }
                let center = CLLocationCoordinate2DMake(minLat + (maxLat - minLat)/2.0, minLon + (maxLon - minLon)/2.0)
                let span = MKCoordinateSpanMake(Double(mapZoom)*1.2*(maxLat - minLat), Double(mapZoom)*1.2*(maxLon - minLon))
                
                let region = MKCoordinateRegionMake(center, span)
                self.setRegion(self.regionThatFits(region), animated: false)
                
                self.updateMagnification()
            }
            firstMapUpdate = false
        }
    }
    
    private var originalTransform : CGAffineTransform? = nil
    ///
    /// zoom the map further my transforming the view...  image res does not increase, but this
    /// makes it easier to draw small things
    private func updateMagnification() {
//        if originalTransform == nil {
//            originalTransform = transform
//        } else {
//            transform = originalTransform!
//        }
//        
//        let s = mapZoom
//        let tr = CGAffineTransformScale(transform, s, s)
//        
//        let h = frame.size.height
//        let w = frame.size.width
//        UIView.animateWithDuration(1.0) { () -> Void in
//            self.transform = tr;
//            self.center = CGPointMake(w-w*s/2, h*s/2)
//        }
    }
    
    ///
    /// pass through
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        return tendDelegate?.mapView?(mapView, viewForAnnotation: annotation)
    }
    
    func mapView(mapView: MKMapView, didAddAnnotationViews views: [MKAnnotationView]) {
        
        for v in views {
            if let s = v.superview {
                s.bringSubviewToFront(v)
            }
        }
    }
    
    
}

///
/// map renderer for regions
///
/// does gradient for data
class RegionRenderer : MKPolygonRenderer {
    
    var defaultFillColor = UIColor.greenColor().colorWithAlphaComponent(0.3)
    let defaultInactiveFillColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
    var region: SensorRegion
    var mapView: MKMapView
    
    init(mapView: MKMapView, overlay: MKOverlay, region: SensorRegion) {
        self.region = region
        self.mapView = mapView
        super.init(overlay: overlay)
        // defaults
        strokeColor = Util.colorBrown
        fillColor = defaultFillColor
        lineWidth = 2.0
    }
    
    
    override func drawMapRect(mapRect: MKMapRect, zoomScale: MKZoomScale, inContext context: CGContext) {
        if !MKMapRectIntersectsRect(self.overlay.boundingMapRect, mapRect) {
            return
        }
        
        super.drawMapRect(mapRect, zoomScale: zoomScale, inContext: context)
    }
    

    var active = true {
        didSet {
            fillColor = active ? defaultFillColor : defaultInactiveFillColor
        }
    }
    var editing = false {
        didSet {
            strokeColor = editing ? Util.colorOrange : Util.colorBrown
        }
    }
    ///
    /// if a date is set, will get the sensor data for this date and render the values
    var date: NSDate?
    var dataIndex : Int? // select which value is rendered - see data_* constants
    
    override func fillPath(path: CGPath, inContext context: CGContext) {
        if let date = date , _ = dataIndex {
         
            let items = SensorData.instance.itemsForRegion(region, date: date)
            if items.isEmpty {
                fillColor = defaultInactiveFillColor
                // just fill
                super.fillPath(path, inContext: context)
            } else {
                fillPath(path, inContext: context, items: items)
            }
            
        } else { // just fill
            super.fillPath(path, inContext: context)
        }
    }
    
    func fillPath(path: CGPath, inContext context: CGContext, items: [SensorDataItem]) {
        // data type... e.g. ec, vwc, etc
        let dataType = dataTypes[dataIndex!]

        let min = dataType.min //minValue(dataIndex)
        let max = dataType.max //maxValue(dataIndex)
        
        // covert to an array of point-UNnormalized_value mappings
        var dataPoints = [ (point: CGPoint, value: Double)]()
        for i in items {
            if let lat = i.latitude, lon = i.longitude {
                
                let mp = MKMapPointForCoordinate(CLLocationCoordinate2DMake(lat, lon))
                let cgp = pointForMapPoint(mp)
                //                let cgp = mapView.convertCoordinate(CLLocationCoordinate2DMake(lat, lon), toPointToView: mapView)
                
                let value = i[dataIndex!] // straight up value, not scaled etc
                dataPoints.append( (point: cgp, value: value) )
            }
        }

        
        var value = 0.0
        
        // average
        for i in items {
            value += i[dataIndex!]
        }
        value /= Double(items.count)
     
        
        // color average
        let color = dataType.gradientColor(value).colorWithAlphaComponent(0.8)
        
        fillColor = color
        //super.fillPath(path, inContext: context)
        
        CGContextSaveGState(context)
        
        // create a path and clip
        CGContextAddPath(context, path)
        CGContextClip(context)

        
        // now fill the whole bounding rect
        let r = self.overlay.boundingMapRect

        let cgr = self.rectForMapRect(r)

        let inc = CGFloat(20.0)
        
        for var x = cgr.origin.x; x < (cgr.origin.x + cgr.width); x += inc {
            for var y = cgr.origin.y; y < (cgr.origin.y + cgr.height); y += inc {
                // calculate color
                let color = heatMapColor(dataType, x: x, y: y, data: dataPoints)
                CGContextSetFillColorWithColor(context, color.CGColor)
                let r = CGRectMake(x, y, inc+0.1, inc+0.1)
                CGContextFillRect(context, r)
            }
        }
        

        // restore state - clip
        CGContextRestoreGState(context)

        // draw text
        CGContextSetTextMatrix(context, CGAffineTransformMakeScale(1.0, -1.0))
        CGContextSetLineWidth(context, 1.5)
        let aFont = UIFont(name: "Optima-Bold", size: 50)

        // region label
        
        // find the north most coord
        var maxLat = 0.0
        var maxLatCoord : CLLocationCoordinate2D? = nil
        for c in region.coords {
            if maxLatCoord==nil || c.latitude > maxLat {
                maxLat = c.latitude
                maxLatCoord = c
            }
        }
        let maxMapPoint = MKMapPointForCoordinate(maxLatCoord!)
        let cgp = pointForMapPoint(maxMapPoint)
        
        func drawCenteredText(cgp: CGPoint, text: String) {

            var attr : [String : AnyObject] = [NSFontAttributeName : aFont!]
            let size = (text as NSString).sizeWithAttributes(attr)
            let x = cgp.x - size.width/2
            let y = cgp.y
            
            // white fill
            CGContextSetTextPosition(context, x, y)
            attr[NSForegroundColorAttributeName] = UIColor.whiteColor()
            var text = CFAttributedStringCreate(nil, region.name!, attr)
            var line = CTLineCreateWithAttributedString(text)
            CGContextSetTextDrawingMode(context, CGTextDrawingMode.Fill)
            CTLineDraw(line, context)

            // black outline
            CGContextSetTextPosition(context, x, y)
            attr[NSForegroundColorAttributeName] = UIColor.blackColor()
            text = CFAttributedStringCreate(nil, region.name!, attr)
            line = CTLineCreateWithAttributedString(text)
            CGContextSetTextDrawingMode(context, CGTextDrawingMode.Stroke)
            CTLineDraw(line, context)
        }
        drawCenteredText(cgp, text: region.name!)
        
        //data labels
        for i in items {
            
            if let lat = i.latitude, lon = i.longitude {
                let v = i[dataIndex!]
                
                let mp = MKMapPointForCoordinate(CLLocationCoordinate2DMake(lat, lon))
                let cgp = pointForMapPoint(mp)

                let s = dataType.format(v)

                let x = cgp.x-25
                let y = cgp.y+60
                
                // white fill
                CGContextSetTextPosition(context, x, y)
                var attr:CFDictionaryRef = [NSFontAttributeName:aFont!, NSForegroundColorAttributeName:UIColor.whiteColor()]
                var text = CFAttributedStringCreate(nil, s, attr)
                var line = CTLineCreateWithAttributedString(text)
                CGContextSetTextDrawingMode(context, CGTextDrawingMode.Fill)
                CTLineDraw(line, context)

                // black outline
                CGContextSetTextPosition(context, x, y)
                attr = [NSFontAttributeName:aFont!, NSForegroundColorAttributeName:UIColor.blackColor()]
                text = CFAttributedStringCreate(nil, s, attr)
                line = CTLineCreateWithAttributedString(text)
                CGContextSetTextDrawingMode(context, CGTextDrawingMode.Stroke)
                CTLineDraw(line, context)
            }
        }
        
        
    }
    ///
    /// make a heatmap by assigning a color to each x,y based on its proximity to the sensor readings
    private func heatMapColor(dataType: DataType, x: CGFloat, y: CGFloat, data: [ (point: CGPoint, value: Double)]) -> UIColor {

        if data.count == 1 {
            return dataType.gradientColor(data[0].value)
        }
        
        
        var r : CGFloat = 0.0
        var g : CGFloat = 0.0
        var b : CGFloat = 0.0
        
        var dSum = CGFloat(0.0)
        for d in data {

            let dx = x-d.point.x
            let dy = y-d.point.y
            let d = sqrt(dx*dx + dy*dy)
            dSum += d
        }
        
        for dd in data {
            let color: UIColor = dataType.gradientColor(dd.value)
            let cc = CGColorGetComponents(color.CGColor)
            //print("color = \(cc[0]), \(cc[1]), \(cc[2])")
            let dx = x-dd.point.x
            let dy = y-dd.point.y
            let d = sqrt(dx*dx + dy*dy)

            r += (1.0/d) * cc[0]
            g += (1.0/d) * cc[1]
            b += (1.0/d) * cc[2]
        }
        
        let m = sqrt( r*r + g*g + b*b)
        r /= m
        g /= m
        b /= m
        
        //print("(\(x), \(y)): \(r), \(g), \(b)")
        
        return UIColor(red: r, green: g, blue: b, alpha: 1.0)
        
    }
}



///
/// translucent tool bar over map
class MapToolbar : UIView {
    
    var actions = [String]() {
        didSet {
            layoutIfNeeded()
            layoutSubviews()
        }
    }
    
    var text: String? = "Regions" {
        didSet {
            if label == nil {
                return
            }
            if text == nil {
                label!.hidden = true
                
            } else {
                label!.hidden = false
                label!.text = text!
            }
        }
    }
    
    var label : UILabel?
    var buttons : [UIButton]!
    
    override func layoutSubviews() {
        let pad = CGFloat(5.0)
        // remove current
        for b in subviews {
            b.removeFromSuperview()
        }
        
        // label
        if label == nil {
            label = UILabel()
            label!.font = UIFont.systemFontOfSize(15.0)
            label!.textColor = UIColor.whiteColor()
            label!.text = ""
            label?.hidden = true
        }
        addSubview(label!)
        label!.sizeToFit()
        let labelH = label!.hidden ? -pad : label!.frame.size.height
        
        label!.frame = CGRectMake(pad, pad, frame.width-pad-pad, label!.frame.height)
        
        // make more
        buttons = [UIButton]()
        if actions.count == 0 {
            return
        }
        let w = (frame.size.width - CGFloat(actions.count+1)*pad) / CGFloat(actions.count)
        let h = frame.size.height - pad - pad - pad - labelH
        var i = CGFloat(0.0)
        for a in actions {
            let b = UIButton()
            b.setTitle(a, forState: UIControlState.Normal)
            b.frame = CGRectMake(pad*(i+1) + w*i, pad + pad + labelH, w, h)
            b.addTarget(self, action: Selector("buttonTouched:"), forControlEvents: UIControlEvents.TouchUpInside)
            addSubview(b)
            if actions.count > 1 {
                b.layer.borderWidth = 1.0
                b.layer.borderColor = Util.colorOrange.CGColor
            }
            i += 1.0
        }
        
    }
    
    var buttonPressed : ((action: String)->())?
    
    func buttonTouched(button: UIButton) {
        buttonPressed?(action: button.titleLabel!.text!)
    }
    
}

///
/// transparent overlay to intercept touches for drawing polygons
class DrawingView : UIView {
    
    var addTouch : ((point: CGPoint)->())?
    var moveTouch : ((point: CGPoint)->())?
    var pointInside : ((point: CGPoint)->Bool)?
    
    
    override func pointInside(point: CGPoint, withEvent event: UIEvent?) -> Bool {
        if let f = pointInside {
            return f(point: point)
        }
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        for t in touches {
            addTouch?(point: t.locationInView(self))
        }
    }
    
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        
    }
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for t in touches {
            moveTouch?(point: t.locationInView(self))
        }
    }
    
}

///
/// exentions to determine if point is inside
extension MKMultiPoint {
    func pointInside(point: CLLocationCoordinate2D) -> Bool {
        let mapPoint = MKMapPointForCoordinate(point)
        let cgPoint = CGPointMake(CGFloat(mapPoint.x), CGFloat(mapPoint.y))
        
        // path
        let mpr = CGPathCreateMutable()
        
        for var i = 0; i<pointCount; i++ {
            let p = points()[i]
            if i == 0 {
                CGPathMoveToPoint(mpr, nil, CGFloat(p.x), CGFloat(p.y))
            } else {
                CGPathAddLineToPoint(mpr, nil, CGFloat(p.x), CGFloat(p.y))
            }
        }
        
        return CGPathContainsPoint(mpr, nil, cgPoint, false)
    }
}