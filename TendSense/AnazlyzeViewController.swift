//
//  AnazlyzeViewController.swift
//  TendSense
//
//  Created by Scott Chalfant on 11/4/15.
//  Copyright © 2015 Tend. All rights reserved.
//

import Foundation
import MapKit


import UIKit

class AnalyzeViewController : UIViewController, MKMapViewDelegate {
    
    @IBOutlet weak var expandCollapseButton: UIButton!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var dataSegmented: UISegmentedControl!
    @IBOutlet weak var mapView: TendMapView!
    @IBOutlet weak var dateStepper: UIStepper!
    
    @IBOutlet weak var settingsView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var settingsViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var scaleView: ScaleView!
    private var expanded : Bool = true
    
    override func viewDidLoad() {
        mapView.tendDelegate = self
        mapView.mapZoom = 1.0
        
    }
    
    private var settingsViewHeightConstant : CGFloat! = 79.0
    
    
    override func viewDidAppear(animated: Bool) {
        //settingsViewHeightConstant = settingsViewHeight.constant

        // remember!
        let defaults = NSUserDefaults.standardUserDefaults()
//        let analyze_expanded = defaults.boolForKey("analyze_expanded")
//        let index = defaults.integerForKey("analyze_dataIndex")
//        dataSegmented.selectedSegmentIndex = index
//        if expanded != analyze_expanded {
//            NSTimer.schedule(0.5, f: { () -> () in
//                self.toggleExpanded()
//            })
//
//        }
        
        
        // config
        dateStepper.maximumValue = Double(SensorData.instance.dates.count - 1)
        dateStepper.value = dateStepper.maximumValue
        
        refresh()
    }
    
    
    private var items : [SensorDataItem]!
    
    
    private func minValue(data: Int) -> Double {
        var res = 0.0
        for i in items {
            let v = i[data]
            
            if i === items.first || v < res {
                res = v
            }
        }
        
        return res
    }
    private func maxValue(data: Int) -> Double {
        var res = 0.0
        for i in items {
            let v = i[data]
            
            if i === items.first || v > res {
                res = v
            }
        }
        
        return res
    }
    
    ///
    /// redraw based on the current config and sensor data
    private func refresh() {
        // clear
        for a in mapView.annotations {
            mapView.removeAnnotation(a)
        }

        // data element
        let dataIndex = dataSegmented.selectedSegmentIndex
        let dataTitle = dataSegmented.titleForSegmentAtIndex(dataIndex)!

        // update scale view
        let dataType = dataTypes[dataIndex]
        scaleView.dataType = dataType

        // no data to show?
        if SensorData.instance.items.isEmpty {
            headerLabel.text = "No Data Recorded"
            dateLabel.text = ""
            mapView.showDataForDate = nil
            
            return
        }
        
        // selected date
        let dateIndex = Int(dateStepper.value)
        let date = SensorData.instance.dates[dateIndex]

        
        // store preference
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setInteger(dataIndex, forKey: "analyze_dataIndex")
        
        
        // note that the readings for regions will be handled by the map view
        // tell the map view
        mapView.showDataForDate = date // this wil trigger refresh of map view
        mapView.showDataForDataIndex = dataIndex
        
        // all items for the current date
        items = SensorData.instance.itemsForDate(date)
        
        
        headerLabel.text = "\(dataTitle) for \(SensorData.instance.dayDateFormatter.stringFromDate(date))"
        dateLabel.text = "\(SensorData.instance.dayDateFormatter.stringFromDate(date)) (\(items.count) samples)"
        
        var index = 0
        
        let itemsWithout = SensorData.instance.itemsWithoutRegions(date)
        
        // add annotations
        for item in items {
            if item.hasCoordinate {
                mapView.addAnnotation(SensorDataAnnotation(data: item, viewController: self))
                
                // if not in a region, add a circle
                if itemsWithout.contains( {$0 === item} ) {
                    let circle = MKCircle(centerCoordinate: CLLocationCoordinate2DMake(item.latitude!, item.longitude!), radius: 10.0)
                    circle.title = "\(index++)"
                    mapView.addOverlay(circle)
                }
            }
        }

    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        
        
        let index = Int(overlay.title!!)
        let item = items[index!]
        
        let dataIndex = dataSegmented.selectedSegmentIndex
        let dataType = dataTypes[dataIndex]

        let value = item[dataIndex]

        let renderer = DataPointRenderer(dataType: dataType, overlay: overlay, value: value)
        
        renderer.fillColor = UIColor.redColor() // UIColor.colorWithAppColor(Util.AppColors.ORANGE_ACCENT)
        renderer.lineWidth = 0
        
        return renderer
    }
    
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        //return MKPinAnnotationView(annotation: annotation, reuseIdentifier: annotation.title!)
        
        if let annotation = annotation as? SensorDataAnnotation {
            let v = DataPointAnnotationView(annotation: annotation)
            v.viewController = self
            return v
        }
        return nil
    }
    
    @IBAction func collapsedButtonTouched(sender: UIButton) {
        toggleExpanded()
    }
    
    func toggleExpanded() {
        if expanded { // collapse
            settingsViewHeight.constant = 0
            settingsView.hidden = true
        } else {
            settingsViewHeight.constant = settingsViewHeightConstant
        }
        
        UIView.beginAnimations(nil, context: nil)
        view.setNeedsUpdateConstraints()
        view.layoutIfNeeded()
        UIView.commitAnimations()

        NSTimer.schedule(0.3, f: { () -> () in
            if self.expanded { // collapse
                self.expandCollapseButton.setImage(UIImage(named: "down_arrow"), forState: UIControlState.Normal)
            } else {
                self.settingsView.hidden = false
                self.expandCollapseButton.setImage(UIImage(named: "up_arrow"), forState: UIControlState.Normal)
            }
            
            self.expanded = !self.expanded
            
            // store preference
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setBool(self.expanded, forKey: "analyze_expanded")

        })
        

    }
    @IBAction func dateStepperChanged(sender: AnyObject) {
        refresh()
    }
    @IBAction func dataSegmentedChanged(sender: AnyObject) {
        refresh()
    }
    
    var historyLat : Double?
    var historyLong: Double?
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "historySegue" {
            
            (segue.destinationViewController as! HistoryViewController).latitude = historyLat
            (segue.destinationViewController as! HistoryViewController).longitude = historyLong
        }
    }
    
    
}

class DataPointAnnotationView : MKAnnotationView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    var viewController : AnalyzeViewController!
    
    init(annotation: MKAnnotation?) {
        super.init(annotation: annotation, reuseIdentifier: "leaf")
        image = UIImage(CGImage: UIImage(named:  "icon_annotation")!.CGImage!, scale: 1.5, orientation: UIImageOrientation.Down)
//        image = UIImage(named:  "icon_stem_leaf")
        canShowCallout = true
        calloutOffset = CGPoint(x: -5, y: 5)
        centerOffset = CGPoint(x: 0.15*image!.size.width, y: -0.25*image!.size.height)

        if let data = annotation as? SensorDataAnnotation  {
            let history = SensorData.instance.historyForLocation(data.data.latitude!, longitude: data.data.longitude!, radius:HISTORY_RADIUS)
            if history.count > 1 {
                // TODO:  highlight somehow
                image = UIImage(CGImage: UIImage(named:  "icon_annotation")!.CGImage!, scale: 1.5, orientation: UIImageOrientation.Down)
                let button = UIButton(frame: CGRectMake(0, 0, 90, 30))
                button.backgroundColor = Util.colorOrange
                button.titleLabel?.font = UIFont.boldSystemFontOfSize(12)
                button.setTitle("History (\(history.count))", forState: UIControlState.Normal)
                button.addTarget(self, action: Selector("buttonTouched"), forControlEvents: UIControlEvents.TouchUpInside)
                rightCalloutAccessoryView = button
            }
        }
    }

    func buttonTouched() {
        if let data = annotation as? SensorDataAnnotation  {
            let history = SensorData.instance.historyForLocation(data.data.latitude!, longitude: data.data.longitude!, radius: HISTORY_RADIUS)
            
            print("history: \(history.count) item(s)")
            viewController.historyLat = data.data.latitude
            viewController.historyLong = data.data.longitude
            
            viewController.performSegueWithIdentifier("historySegue", sender: self)
            
            
        }
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
}

///
/// render a data point
class DataPointRenderer : MKCircleRenderer {

    init(dataType: DataType, overlay: MKOverlay, value: Double) {
        self.value = value
        self.dataType = dataType
        super.init(overlay: overlay)
    }
    
    var dataType : DataType

    // the actual unscaled value
    var value : Double
    
    
    override func fillPath(path: CGPath, inContext context: CGContext) {
        
        let rect = CGPathGetBoundingBox(path)
        
        CGContextAddPath(context, path)
        CGContextClip(context)
        
        // one location for each color, 0 is center, 1 is perimeter
        let gradientLocations : [CGFloat] = [0.4, 1.0]

        // Start color r,g,b,a
        // End color r,g,b,a
        let c = dataType.gradientColor(value)
        let cc = CGColorGetComponents(c.CGColor)
        
        let gradientColors : [CGFloat] = [cc[0], cc[1], cc[2], 0.6, 0.0, 0.0, 0.0, 0.0]

        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let gradient = CGGradientCreateWithColorComponents(colorSpace, gradientColors, gradientLocations, 2)
        
        let gradientCenter = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect))
        let gradientRadius = min(rect.size.width, rect.size.height) / 2
        
        CGContextDrawRadialGradient(context, gradient, gradientCenter, 0, gradientCenter, gradientRadius, CGGradientDrawingOptions.DrawsAfterEndLocation)
    }
    
}

class SensorDataAnnotation : NSObject, MKAnnotation {
    init(data: SensorDataItem, viewController: AnalyzeViewController) {
        self.data = data
        self.viewController = viewController
        super.init()
    }
    var data: SensorDataItem
    var viewController: AnalyzeViewController
    
    var title: String? {
    
        let dataIndex = viewController.dataSegmented.selectedSegmentIndex
        let dataTitle = viewController.dataSegmented.titleForSegmentAtIndex(dataIndex)!
        let dataType = dataTypes[dataIndex]

        let t = data[dataIndex]
        let s = dataType.format(t)
        return "\(dataTitle)=\(s)"

    }
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: data.latitude!, longitude: data.longitude!)
    }

    
    
}

///
/// show the data scale with gradient and value labels for min, mid and max
public class ScaleView : UIView {
    
    var dataType: DataType! {
        didSet {
            setNeedsDisplay()
        }
    }
    

    
    private func drawBackground(rect: CGRect) {
        
        
        if dataType == nil {
            return
        }
        
        let colors = dataType.scaleColors
        
        // one location for each color, 0 is center, 1 is perimeter
        var gradientLocations = [CGFloat]()
        var x = CGFloat(0.0)
        for _ in colors {
            gradientLocations.append(x)
            x += CGFloat(1.0 / Double(colors.count-1))
        }
        
        
        // each color r,g,b,a
        var gradientColors = [CGFloat]()
        for c in colors {
            let rgba = CGColorGetComponents(c.CGColor)
            gradientColors.append(rgba[0])
            gradientColors.append(rgba[1])
            gradientColors.append(rgba[2])
            gradientColors.append(rgba[3])
        }
        
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let gradient = CGGradientCreateWithColorComponents(colorSpace, gradientColors, gradientLocations, colors.count)
        
        
        let ctx = UIGraphicsGetCurrentContext()

        CGContextDrawLinearGradient(ctx, gradient, CGPointMake(0, 0), CGPointMake(rect.width, 0), CGGradientDrawingOptions.DrawsAfterEndLocation)
        
        

    }
    
    
    
    public override func updateConstraints() {
        super.updateConstraints()
        
        // is this a good place to do this?
        UIDevice.currentDevice().beginGeneratingDeviceOrientationNotifications()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("orientationChanged"), name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    func orientationChanged() {
        setNeedsDisplay()
    }
    
    private var leftLabel : UILabel!
    private var rightLabel : UILabel!
    private var midLabel : UILabel!
    
    
    private var lastWidth : CGFloat = 0.0
    
    override public func drawRect(rect: CGRect) {
        super.drawRect(rect)
        if dataType == nil {
            return
        }

        drawBackground(rect)

        // hacky way to see if screen rotated
        let needsNewLabels = lastWidth != rect.width

        if leftLabel == nil || needsNewLabels  {
            if leftLabel != nil { leftLabel.removeFromSuperview() }
            leftLabel = UILabel(frame: CGRectMake(1, 0, rect.width/2, rect.height))
            leftLabel.font = UIFont.boldSystemFontOfSize(12)
            leftLabel.textColor = UIColor.whiteColor()
            leftLabel.textAlignment = NSTextAlignment.Left
            leftLabel.shadowColor = UIColor.blackColor()
            addSubview(leftLabel)
        }

        leftLabel.text = dataType.format(dataType.min)
        
        
        if rightLabel == nil || needsNewLabels {
            if rightLabel != nil { rightLabel.removeFromSuperview() }
            rightLabel = UILabel(frame: CGRectMake(0, 0, rect.width-1, rect.height))
            rightLabel.font = UIFont.boldSystemFontOfSize(12)
            rightLabel.textColor = UIColor.whiteColor()
            rightLabel.textAlignment = NSTextAlignment.Right
            rightLabel.shadowColor = UIColor.blackColor()
            addSubview(rightLabel)
        }

        rightLabel.text = dataType.format(dataType.max)
        
        if midLabel == nil || needsNewLabels  {
            if midLabel != nil { midLabel.removeFromSuperview() }
            midLabel = UILabel(frame: CGRectMake(0, 0, rect.width, rect.height))
            midLabel.font = UIFont.boldSystemFontOfSize(12)
            midLabel.textColor = UIColor.whiteColor()
            midLabel.textAlignment = NSTextAlignment.Center
            midLabel.shadowColor = UIColor.blackColor()
            //addSubview(midLabel)
        }

        midLabel.text = dataType.format( dataType.min + (dataType.max-dataType.min)/2.0 )

        lastWidth = rect.width
    }
}

