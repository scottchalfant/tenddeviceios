//
//  SensorManager.swift
//  TendSense
//
//  Created by Scott Chalfant on 11/4/15.
//  Copyright © 2015 Tend. All rights reserved.
//

import Foundation
import CoreBluetooth


func log(msg: String) {
    print("\(NSDate()): \(msg)")
}

protocol SensorManagerListener {
    
    func sensorManagerFoundSensor(sensor: Sensor)
    
    func sensorManagerError(message: String)
    
    func sensorManagerReady()
}



protocol SensorListener {
    func sensorDataUpdated(sensor: Sensor)
    func sensorReadStarting(sensor: Sensor)
    func sensorReadComplete(sensor: Sensor)
    
    func sensorBatchTransferStarting(sensor: Sensor, count: Int)
    func sensorBatchTransferComplete(sensor: Sensor)
    
    func sensorConnectStatus(sensor: Sensor, connected: Bool)
    func sensorError(sensor: Sensor, message: String)
    
}

class Sensor : NSObject, CBPeripheralDelegate {
  
    init(peripheral: CBPeripheral) {
        self.peripheral = peripheral
        self.name = peripheral.name
        self.id = peripheral.identifier.UUIDString
        super.init()
        peripheral.delegate = self
    }
    
    var name: String?
    var id: String?
    
    var connected: Bool = false
    
    var listeners = [SensorListener]()
    
    var peripheral : CBPeripheral
    
    // timestamp
    var timestamp : NSDate? = nil
    
    // data elements
    var airTemperature: Double? = nil // deg C
    var humidity: Double? = nil    // percent

    // location
    var latitude: Double? = nil
    var longitude: Double? = nil
    var altitude: Double? = nil
    
    // soil
    var ec: Double? = nil // dS/m
    var vwc: Double? = nil
    var soilTemperature: Double? = nil // deg C
    var period: Double? = nil
    var perm: Double? = nil
    var voltRatio: Double? = nil
    
    // lifescycle
    func connect() {
        if connected {
            return
        }
        log("connecting to \(name)")
        SensorManager.instance.connect(self)
    }
    func disconnect() {
        if !connected {
            return
        }
        log("disconnecting from \(name)")
        SensorManager.instance.disconnect(self)
    }
    
    func read() {
        if !connected {
            return
        }

        // TODO: do read
        
    }
    
    private func didConnect() {
        connected = true
        for l in listeners {
            l.sensorConnectStatus(self, connected: true)
        }
    }
    
    private func didDisconnect() {
        connected = false
        for l in listeners {
            l.sensorConnectStatus(self, connected: false)
        }
    }
    private func error(e: String) {
        for l in listeners {
            l.sensorError(self, message: e)
        }
    }
    
    var service: CBService?
    
    func peripheral(peripheral: CBPeripheral, didDiscoverServices error: NSError?) {
        if error != nil {
            log("didDiscoverServices \(error)")
        } else {
            // only 1
            for s in peripheral.services! {
                service = s
            }
            
            peripheral.discoverCharacteristics(nil , forService: service!)
        }
        
    }
    
    var txChar : CBCharacteristic?
    var rxChar : CBCharacteristic?

    func peripheral(peripheral: CBPeripheral, didDiscoverCharacteristicsForService service: CBService, error: NSError?) {
        if error != nil {
            log("didDiscoverCharacteristicsForService \(error)")
        } else {
            for c in service.characteristics! {
                if c.UUID.UUIDString == "713D0002-503E-4C75-BA94-3148F18D941E" {
                    txChar = c
                }
                if c.UUID.UUIDString == "713D0003-503E-4C75-BA94-3148F18D941E" {
                    rxChar = c
                }
            }
        }
        log("service: \(service)")
        log("txChar:  \(txChar)")
        log("rxChar:  \(rxChar)")
        
        peripheral.setNotifyValue(true, forCharacteristic: txChar!)
        
        didConnect()
    }
 
    func peripheral(peripheral: CBPeripheral, didUpdateNotificationStateForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
        log("didUpdateNotificationStateForCharacteristic \(error)")
    }
    
    var prevData : NSData?
    
    func peripheral(peripheral: CBPeripheral, didUpdateValueForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
        
        log("didUpdateValueForCharacteristic: \(characteristic.value?.hexadecimalString)")
        
        if characteristic.value != nil {
            let data = characteristic.value!
            backgroundThread(0.0, background: { () -> Void in
                self.processData(data)
            })
            
        }
    }
    func processData(var data: NSData) {
        // was there some left over?
        if let prev = prevData {
            let newdata = NSMutableData()
            newdata.appendData(prev)
            newdata.appendData(data)
            data = newdata
            log(" pre-pended \(prev.length) bytes to make \(newdata.hexadecimalString)")
        }
        
        // read 6-byte records until there's no more!
        var b = consumeFromData(data)
        while b {
            // each record is 6 bytes, move up that many
            let range = NSMakeRange(6, data.length-6)
            data = data.subdataWithRange(range)
            b = consumeFromData(data)
        }
        // is there any left?
        if data.length > 0 {
            log("  \(data.length) bytes left over")
            prevData = data
        } else {
            prevData = nil
        }
    }

    func mainThread(main: (()->())) {
        dispatch_async(dispatch_get_main_queue(), {
            main()
        })
        
    }
    func backgroundThread(delay: Double = 0.0, background: (() -> Void)? = nil, completion: (() -> Void)? = nil) {

        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)) {
            if(background != nil){ background!(); }
            
            let popTime = dispatch_time(DISPATCH_TIME_NOW, Int64(delay * Double(NSEC_PER_SEC)))
            dispatch_after(popTime, dispatch_get_main_queue()) {
                if(completion != nil){ completion!(); }
            }
        }
    }
    
    ///
    /// each record is 6 bytes
    func consumeFromData(value: NSData) -> Bool {
        if value.length < 6 {
            return false
        }
        
        if let code = value.twoAsString {
            log("code: \(code)")
            
            if code == "RD" {
                // read starting
                
                // RESET  data values
                // timestamp
                timestamp = nil
                
                // data elements
                airTemperature = nil // deg C
                humidity = nil    // percent
                
                // location
                latitude = nil
                longitude = nil
                altitude = nil
                
                // soil
                ec = nil // dS/m
                vwc = nil
                soilTemperature = nil // deg C
                period = nil
                perm = nil
                voltRatio = nil
                
                
                // signal
                for l in listeners {
                    mainThread({ () -> () in
                        l.sensorReadStarting(self)
                    })
                }
            } else if code == "RC" {
                // read complete
                SensorData.instance.writeCurrentData(self)
                
                // listeners
                for l in listeners {
                    mainThread({ () -> () in
                        l.sensorReadComplete(self)
                    })
                }
            } else if code == "TR" { // starting to receive transfer of local stored data
                
                var count = -1
                if let val = value.lastFourAsFloat {
                    count = Int(val)
                }
                
                // listeners
                for l in listeners {
                    mainThread({ () -> () in
                        l.sensorBatchTransferStarting(self, count: count)
                    })
                }
            } else if code == "TC" { // done with transfer of local stored data
                
                // listeners
                for l in listeners {
                    mainThread({ () -> () in
                        l.sensorBatchTransferComplete(self)
                    })
                }
            } else if code == "DA" { // date time of sample
                
                if let date = value.lastFourAsDate {
                    timestamp = date
                }
                
            } else {

                if let val = value.lastFourAsFloat {
                    log("val: \(val)")
                    switch (code) {
                    case "TA": // temp air
                        airTemperature = Double(val)
                        break
                    case "TS": // temp soil
                        soilTemperature = Double(val)
                        break
                    case "HU": // humidity
                        humidity = Double(val)
                        break
                    case "LA": // lat
                        latitude = Double(val)
                        break
                    case "LN": // lon
                        longitude = Double(val)
                        break
                    case "AL": // altitude
                        altitude = Double(val)
                        break
                    case "VW": // VWC
                        vwc = Double(val)
                        break
                    case "EC": // EV
                        ec = Double(val)
                        break
                    case "PE": // perm
                        perm = Double(val)
                        break
                    case "PD": // period
                        period = Double(val)
                        break
                    case "VR": // voltage ratio
                        voltRatio = Double(val)
                        break
                    default:
                        break
                    }
                    
                    for l in listeners {
                        mainThread({ () -> () in
                            l.sensorDataUpdated(self)
                        })
                    }
                }
            }
            return true
        } // if let
        return false
    }
}

class SensorManager : NSObject, CBCentralManagerDelegate {
    
    static var instance = SensorManager()
    override init() {
        super.init()
        start()
    }
    
    var listeners = [SensorManagerListener]()
    var centralManager : CBCentralManager!
    
    var sensors = [Sensor]()
    
    var ready : Bool {
        return centralManager.state == .PoweredOn
    }
    
    func start() {
        centralManager = CBCentralManager(delegate: self, queue: nil)
        centralManager.delegate = self

    }
    
    func discoverSensors() {
        log("discoverSensors")
        centralManager.scanForPeripheralsWithServices(nil, options: nil)
    }

    func stopDiscoveringSensors() {
        log("stopDiscoveringSensors")
        centralManager.stopScan()
    }
    


    var lastSensor : Sensor? {
        
        get {
            
            let defaults = NSUserDefaults.standardUserDefaults()
            if let uuid = defaults.objectForKey("lastUUID") as? String where uuid != "" {
                for p in centralManager.retrievePeripheralsWithIdentifiers([NSUUID(UUIDString: uuid)!]) {
                    if let res = sensorForPeripheral(p) {
                        return res
                    } else {
                        let sensor = Sensor(peripheral: p)
                        sensors.append(sensor)
                        return sensor
                    }
                }
            }
            return nil
            
        }
        set {
            
            let uuid = newValue == nil ? "" : newValue?.peripheral.identifier.UUIDString
            
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject(uuid, forKey: "lastUUID")
            
        }
        
    }
    
    
    func centralManagerDidUpdateState(central: CBCentralManager) {
        
        let ok = central.state == .PoweredOn
        log("centralManagerDidUpdateState \(central.state.rawValue) ok = \(ok)")
        
        if ok {
            for l in listeners {
                l.sensorManagerReady()
            }
        }
    }
    func centralManager(central: CBCentralManager, didDiscoverPeripheral peripheral: CBPeripheral, advertisementData: [String : AnyObject], RSSI: NSNumber) {
        
        log("didDiscoverPeripheral \(peripheral)")
        
        if let n = peripheral.name where n.hasPrefix("Tend") || n.hasPrefix("Blend") {
            let sensor = Sensor(peripheral: peripheral)
            sensors.append(sensor)
            for l in listeners {
                l.sensorManagerFoundSensor(sensor)
            }
        }
        
    }
    
    func centralManager(central: CBCentralManager, didFailToConnectPeripheral peripheral: CBPeripheral, error: NSError?) {
        log("didFailToConnectPeripheral \(error)")
        for l in listeners {
            l.sensorManagerError("Error connecting.  \(error)")
        }
        if let sensor = sensorForPeripheral(peripheral) {
            sensor.error("Error connecting.  \(error)")
        }
    }
    
    func centralManager(central: CBCentralManager, didConnectPeripheral peripheral: CBPeripheral) {
        if let _ = sensorForPeripheral(peripheral) {
            
            peripheral.discoverServices([CBUUID(string: "713D0000-503E-4C75-BA94-3148F18D941E")])
            
        } else {
            log("didConnectPeripheral cannot find sensor for peripheral: \(peripheral)")
        }
    }
    func centralManager(central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: NSError?) {
        if let sensor = sensorForPeripheral(peripheral) {
            sensor.didDisconnect()
        } else {
            log("didDisconnectPeripheral cannot find sensor for peripheral: \(peripheral)")
            if let sensor = sensorForPeripheral(peripheral) {
                sensor.error("Error disconnecting (1).")
            }
        }
    }
    private func sensorForPeripheral(p: CBPeripheral) -> Sensor? {
        for s in sensors {
            if s.peripheral === p {
                return s
            }
        }
        return nil
    }
    
    private func connect(sensor: Sensor) {
        centralManager.connectPeripheral(sensor.peripheral, options: nil)
        
    }
    private func disconnect(sensor: Sensor) {
        centralManager.cancelPeripheralConnection(sensor.peripheral)
        
    }
}

extension NSData {
    
    var hexadecimalString : String {
        let buf = UnsafePointer<UInt8>(bytes)
        let charA = UInt8(UnicodeScalar("a").value)
        let char0 = UInt8(UnicodeScalar("0").value)
        
        func itoh(i: UInt8) -> UInt8 {
            return (i > 9) ? (charA + i - 10) : (char0 + i)
        }
        
        let p = UnsafeMutablePointer<UInt8>.alloc(length * 2)
        
        for i in 0..<length {
            p[i*2] = itoh((buf[i] >> 4) & 0xF)
            p[i*2+1] = itoh(buf[i] & 0xF)
        }
        
        return NSString(bytesNoCopy: p, length: length*2, encoding: NSUTF8StringEncoding, freeWhenDone: true)! as String
    }
    
    var twoAsString : String? {
        
        if length < 2 {
            return nil
        }
        
        var array = [UInt8](count: length, repeatedValue: 0)
        
        // copy bytes into array
        self.getBytes(&array, length:length * sizeof(UInt32))

        return NSString(bytes: &array, length: 2, encoding: NSUTF8StringEncoding) as String?
        
    }
    
    var lastFourAsFloat : Float? {
        
        if length < 4 {
            return nil
        }
        
        var u : UInt32 = 0
        // copy bytes into array
        self.getBytes(&u, range: NSMakeRange(2, 4))

        let floatVal:Float = unsafeBitCast(u, Float.self)
        return floatVal
    }
    
    var lastFourAsDate : NSDate? {
        if length < 4 {
            return nil
        }
        
        var d : DateTime = DateTime()
        
        // copy bytes into array
        self.getBytes(&d, range: NSMakeRange(2, 4))
        
        print("year: \(d.year)")
        print("month: \(d.month)")
        print("day: \(d.day)")
        print("hour: \(d.hour)")
        print("minute: \(d.minute)")
        
        
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm z"
        
        let dateString = "\(2000+d.year)-\(d.month)-\(d.day) \(d.hour):\(d.minute) GMT"

        
        let date2 = formatter.dateFromString(dateString)
        print(date2)
        
        return date2
        
    }
    

}