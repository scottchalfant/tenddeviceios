//
//  SingleBarView.swift
//  TendSense
//
//  Created by Scott Chalfant on 11/4/15.
//  Copyright © 2015 Tend. All rights reserved.
//

import Foundation
import UIKit

public class SingleBarView : UIView {
    

    ///
    var dataType: DataType?
    
    var value : Double?  {
        didSet {
            setNeedsDisplay()
        }
    }
    
    
    ///
    /// fill: fraction to fill, 0.0-1.0
    private func draw(r: CGRect, color: UIColor, fill: Double?) {
        let ctx = UIGraphicsGetCurrentContext()
        
        let line = CGFloat(2.0)
        let lineColor = Util.colorBrown.CGColor
        
        let o = r.origin
        let right = r.width
        
        CGContextSetStrokeColorWithColor(ctx, lineColor)
        CGContextSetFillColorWithColor(ctx, lineColor)
        CGContextSetLineWidth(ctx, line)
        
        // outer rect
        CGContextStrokeRect(ctx, CGRectMake(o.x, o.y, right, r.height))
        
        // percent fill
        if let fill = fill {
            CGContextSetFillColorWithColor(ctx, color.CGColor)
            var w = CGFloat(fill * Double(right-2*line))
            if w > r.width {
                w = r.width
            }
            CGContextFillRect(ctx, CGRectMake(o.x+line, o.y+line, w, r.height - 2*line))
        }
    }
    
    var label: UILabel?
    
    override public func drawRect(rect: CGRect) {
        super.drawRect(rect)
        
        let ctx = UIGraphicsGetCurrentContext()
        CGContextSetFillColorWithColor(ctx, Util.colorBeige.CGColor)
        CGContextFillRect(ctx, rect)
        
        let f = self.frame
        
        let r = CGRectMake(1, 1, f.width-10, f.height-3)
        
        if let v = value, let dataType = dataType {
            let color = dataType.gradientColor(v)
            let p = dataType.normalize(v) //(v-dataType.min) / (dataType.max-dataType.min)
            draw(r, color: color, fill: p)
            
        } else {
            draw(r, color: UIColor.grayColor(), fill: nil)
        }
        
        // label
        if label == nil {
            label = UILabel(frame: CGRectMake(4, 0, self.frame.width, self.frame.height))
            self.addSubview(label!)
        }
        
        if let v = value, dataType = dataType {
            label!.text = dataType.format(v)
        } else {
            label!.text = "--"
        }
    }
}
