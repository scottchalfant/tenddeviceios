//
//  DefineRegionsViewController.swift
//  TendSense
//
//  Created by Scott Chalfant on 1/1/16.
//  Copyright © 2016 Tend. All rights reserved.
//

import Foundation
import UIKit
import MapKit


class DefineRegionsViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapView: TendMapView!

    private var rightBarButton : UIBarButtonItem!
    
    override func viewDidLoad() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.hidden = true
        tableView.editing = true
        
        // map
        mapView.viewController = self
//        mapView.mapZoom = 1.5
        mapView.editable = true
        
        // button
        rightBarButton = UIBarButtonItem(title: "Table", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("toggleMapTable"))
        self.navigationItem.rightBarButtonItem = rightBarButton
        

    }
    
    override func viewWillDisappear(animated: Bool) {
        SensorData.instance.saveRegions() // persist!
    }
    
    
    func toggleMapTable() {
        if rightBarButton.title! == "Table" { // go to table
            tableView.reloadData()
            tableView.hidden = false
            mapView.hidden = true
            mapView.toolbar.hidden = true
            rightBarButton.title = "Map"
            
        } else { // go to map
            tableView.hidden = true
            mapView.hidden = false
            mapView.toolbar.hidden = false
            mapView.refreshRegions()
            rightBarButton.title = "Table"
        }
    }
    
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.Delete
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")!
        

        if let label:UILabel = cell.contentView.viewWithTag(1) as? UILabel {
            label.text = SensorData.instance.regions[indexPath.row].name
        }
        if let label:UILabel = cell.contentView.viewWithTag(2) as? UILabel {
            label.hidden = true // for detail data...
        }
        return cell

    }
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        SensorData.instance.regions.removeAtIndex(indexPath.row)
        tableView.reloadData()
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SensorData.instance.regions.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
}

