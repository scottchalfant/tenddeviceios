//
//  Util.swift
//  TendSense
//
//  Created by Scott Chalfant on 11/4/15.
//  Copyright © 2015 Tend. All rights reserved.
//

import Foundation
import UIKit

class Util {
    
    /*
orange: 207, 137, 91
beige: 242, 240, 235
brown : 84, 75, 53
*/
    static var colorOrange = UIColor(colorLiteralRed: 207.0/255.0, green: 137.0/255.0, blue: 91.0/255.0, alpha: 1.0)
    static var colorBeige = UIColor(colorLiteralRed: 242.0/255.0, green: 240.0/255.0, blue: 235.0/255.0, alpha: 1.0)
    static var colorBrown = UIColor(colorLiteralRed: 84.0/255.0, green: 75.0/255.0, blue: 53.0/255.0, alpha: 1.0)
 
    
    class func hexToColor(hex: String) -> UIColor {
        var hexint : UInt32 = 0
        
        let scanner = NSScanner(string: hex)
        
        scanner.scanHexInt(&hexint)
        
        let r : CGFloat = (CGFloat((hexint & 0xFF0000) >> 16) / 255.0)
        let g : CGFloat = (CGFloat((hexint & 0xFF00) >> 8) / 255.0)
        let b : CGFloat = (CGFloat((hexint & 0xFF)) / 255.0)
        
        return UIColor(red: r, green: g, blue: b, alpha: 1.0)

    }
    
    class func alert(vc: UIViewController, title: String, message: String, buttons: [String],
        result: (buttonPressed: String)->() = { (button) in } ) -> AnyObject {
            print("alert: \(title) \(message) \(buttons)")
            
                let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.ActionSheet)
                
                // buttons
                for button in buttons {
                    let b = button
                    let style = UIAlertActionStyle.Default
                    let action : UIAlertAction = UIAlertAction(title: b, style: style, handler: {alertAction in
                        print("alert: buttonPressed=\(b)")
                        result(buttonPressed: b)
                    })
                    alert.addAction(action)
                }
                
                vc.presentViewController(alert, animated: true, completion: nil)
                return alert

    }
    
}



extension NSTimer {
    
    
    ///
    /// call the function after the interval.  optionally repeat
    class func schedule(ti: NSTimeInterval, f: () -> (), repeats: Bool = false) -> NSTimer {
        let scheduledThingDoer = ScheduledThingDoer(s: f)
        
        return NSTimer.scheduledTimerWithTimeInterval(ti, target: scheduledThingDoer, selector: Selector("doScheduledThing"), userInfo: nil, repeats: repeats)
    }
    
}


class ScheduledThingDoer : NSObject {
    
    init(s: () -> ()) {
        scheduledThing = s
    }
    var scheduledThing : () -> ()
    
    
    func doScheduledThing() {
        scheduledThing()
    }
    
    
}


public extension UIDevice {
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8 where value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,1", "iPad5,3", "iPad5,4":           return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
}
