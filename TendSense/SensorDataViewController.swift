//
//  SensorDataViewController.swift
//  TendSense
//
//  Created by Scott Chalfant on 11/4/15.
//  Copyright © 2015 Tend. All rights reserved.
//

import Foundation
import UIKit


class SensorDataViewController : UITableViewController {
    
    
    @IBOutlet weak var barEC: SingleBarView!
    @IBOutlet weak var barVWC: SingleBarView!
    @IBOutlet weak var barAirTemp: SingleBarView!
    @IBOutlet weak var barSoilTemp: SingleBarView!
    @IBOutlet weak var barHumidity: SingleBarView!

    override func viewDidLoad() {
        barEC.dataType = dataTypes[data_EC]
        barVWC.dataType = dataTypes[data_VWC]
        barAirTemp.dataType = dataTypes[data_AIRT]
        barSoilTemp.dataType = dataTypes[data_SOILT]
        barHumidity.dataType = dataTypes[data_HUMIDITY]
    }
    
}